# SMUSHY #

## Analog Support ##
### Analog To Digital ###
**ADS1115 6-Bit ADC**
[https://www.adafruit.com/products/1085](https://www.adafruit.com/products/1085)

Need analog support for some of the sensors below. Using Pi4j's libraries
to interact with ADC. 

Requirements:

  * 3v connection
  * Ground
  * Pins 3 and 5 for I2C 

## Sensors ##
### Distance Sensor ###
**HC-SR04 Ultrasonic Range Sensor**
[https://docs.google.com/document/d/1Y-yZnNhMYy7rwhAgyL_pfa39RsB-x2qR4vP8saG73rE/edit](https://docs.google.com/document/d/1Y-yZnNhMYy7rwhAgyL_pfa39RsB-x2qR4vP8saG73rE/edit)
[https://www.mpja.com/download/hc-sr04_ultrasonic_module_user_guidejohn.pdf](https://www.mpja.com/download/hc-sr04_ultrasonic_module_user_guidejohn.pdf)

Used to detect how far away the actuator is from the bottom. Want to use
it as a safety measure to not destroy the frame. 

Distance sensor requires a voltage divider before sending the echo 
signal back to the raspberry pi. 

Echo wiring:

  * Echo -> 220 Ohms resister -> 1000 Ohms resister -> Wire to Pi
  * Wire to Pi -> 1000 Ohms resister -> 1000 Ohms resister -> Ground

Resister colors:

  * 100 Ohms -> Brown, Black, Brown, Gold
  * 1000 Ohms -> Brown, Black, Red, Gold  

The wire to the pi for the echo is connected to the raspberry pi and
to the ground at the time time. Pictures in Google Drive folder 
Can Crusher. At the time, I don't know if I need separate resisters
or a single one for each leg. 

Current problems:

  * Unable to accurately detect past 3 inches. (Code isn't detecting fast enough)
  * Distance sensor code gets stuck in the trigger state. Need to find a way to reset. 

### Beam Sensor ###
**Adafruit Accessories IR Break Beam Sensor - 5mm LEDs**

Used to detect if a can has been set down on the crusher bottom. 

Recommended to be hooked up to 5v, but can work on 3v. Requires the 
input GPIO to have pull up resistance because the single is 1v at best.

Current problems: None (Tested code and works)

### Current Sensor ###
**Sunkee 30A Range Current Sensor Module ACS712** 
[http://henrysbench.capnfatz.com/henrys-bench/arduino-current-measurements/acs712-current-sensor-user-manual/](http://henrysbench.capnfatz.com/henrys-bench/arduino-current-measurements/acs712-current-sensor-user-manual/)

Used to monitor current being used by the actuator. Another safety
measure to make sure its not pulling to much load. 

## Actuator Control ##
### Relays ###
**SainSmart 2-Channel 5V Relay**
[http://www.sainsmart.com/arduino-pro-mini.html](http://www.sainsmart.com/arduino-pro-mini.html)