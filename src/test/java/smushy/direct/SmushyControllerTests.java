package smushy.direct;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.ManualDirectState;

import static org.junit.Assert.*;
import static smushy.direct.SmushyAction.*;

@RunWith(MockitoJUnitRunner.class)
public class SmushyControllerTests {

  @InjectMocks private SmushyController smushyController;
  @Spy private ManualDirectState manualDirectState = new ManualDirectState();

  @Test
  public void testExtend() throws Exception {
    smushyController.extend();
    assertTrue(manualDirectState.isDirecting());
    assertEquals(CRUSH, manualDirectState.getSmushyAction());
  }

  @Test
  public void testRetract() throws Exception {
    smushyController.retract();
    assertTrue(manualDirectState.isDirecting());
    assertEquals(RETRACT, manualDirectState.getSmushyAction());
  }

  @Test
  public void testStop() throws Exception {
    smushyController.stop();
    assertTrue(manualDirectState.isDirecting());
    assertEquals(STOP, manualDirectState.getSmushyAction());
  }

  @Test
  public void testStopManualDirecting() throws Exception {
    smushyController.stopManualDirecting();
    assertFalse(manualDirectState.isDirecting());
    assertEquals(NOTHING, manualDirectState.getSmushyAction());
  }
}
