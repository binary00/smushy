package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.*;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_ShouldSuppressInitialIndicator {

  @Test
  public void testShouldSuppressInitialIndicatorTrue() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(true);
    when(smushyState.getAlertIndicateTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.isBeamActivated()).thenReturn(false);
    when(smushyState.getBeamUnActivatedTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);

    boolean suppress = shouldSuppressInitialIndicator(smushyState);
    assertTrue(suppress);
  }

  @Test
  public void testShouldSuppressInitialIndicatorNotAlerting() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertIndicateTime()).thenReturn(0);
    when(smushyState.isBeamActivated()).thenReturn(false);
    when(smushyState.getBeamUnActivatedTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);

    boolean suppress = shouldSuppressInitialIndicator(smushyState);
    assertFalse(suppress);
  }

  @Test
  public void testShouldSuppressInitialIndicatorBeamActivated() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(true);
    when(smushyState.getAlertIndicateTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamUnActivatedTime()).thenReturn(0);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);

    boolean suppress = shouldSuppressInitialIndicator(smushyState);
    assertFalse(suppress);
  }

  @Test
  public void testShouldSuppressInitialIndicatorActuating() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(true);
    when(smushyState.getAlertIndicateTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.isBeamActivated()).thenReturn(false);
    when(smushyState.getBeamUnActivatedTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.getActuatorIdleTime()).thenReturn(0);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(3D);
    when(smushyState.getActuatorDistance()).thenReturn(10D);

    boolean suppress = shouldSuppressInitialIndicator(smushyState);
    assertFalse(suppress);
  }
}
