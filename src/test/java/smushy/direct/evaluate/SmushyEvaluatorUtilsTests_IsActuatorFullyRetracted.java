package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.*;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_IsActuatorFullyRetracted {

  @Test
  public void testActuatorFullyRetractedNull() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT + 1);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);
    when(smushyState.getActuatorDistance()).thenReturn(null);

    boolean retracted = isActuatorFullyRetracted(smushyState);
    assertFalse(retracted);
  }

  @Test
  public void testActuatorFullyRetractedHasCurrentLoad() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT + 1);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean retracted = isActuatorFullyRetracted(smushyState);
    assertFalse(retracted);
  }

  @Test
  public void testActuatorFullyRetractedActuating() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean retracted = isActuatorFullyRetracted(smushyState);
    assertFalse(retracted);
  }

  @Test
  public void testActuatorFullyRetractedFalse() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE - 1);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean retracted = isActuatorFullyRetracted(smushyState);
    assertFalse(retracted);
  }

  @Test
  public void testActuatorFullyRetractedTrue() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean retracted = isActuatorFullyRetracted(smushyState);
    assertTrue(retracted);
  }

  @Test
  public void testActuatorFullyRetractedEquals() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean retracted = isActuatorFullyRetracted(smushyState);
    assertFalse(retracted);
  }
}
