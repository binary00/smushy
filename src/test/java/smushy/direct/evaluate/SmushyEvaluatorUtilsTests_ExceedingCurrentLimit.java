package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.MAX_CURRENT_THRESHOLD;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.exceedingCurrentLimit;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_ExceedingCurrentLimit {

  @Test
  public void testExceedingCurrentLimitNull() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(null);

    boolean exceeded = exceedingCurrentLimit(smushyState);
    assertFalse(exceeded);
  }

  @Test
  public void testExceedingCurrentLimitLessThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(MAX_CURRENT_THRESHOLD - 1);

    boolean exceeded = exceedingCurrentLimit(smushyState);
    assertFalse(exceeded);
  }

  @Test
  public void testExceedingCurrentLimitGreaterThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(MAX_CURRENT_THRESHOLD + 1);

    boolean exceeded = exceedingCurrentLimit(smushyState);
    assertTrue(exceeded);
  }

  @Test
  public void testExceedingCurrentLimitEquals() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(MAX_CURRENT_THRESHOLD);

    boolean exceeded = exceedingCurrentLimit(smushyState);
    assertFalse(exceeded);
  }
}
