package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.GLOBAL_REST_TIME;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.metBeamUnActivatedThreshold;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_MetBeamUnActivatedThreshold {

  @Test
  public void testMetBeamUnActivatedNotSet() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);

    boolean met = metBeamUnActivatedThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testMetBeamUnActivatedWithActivation() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamUnActivatedTime()).thenReturn(GLOBAL_REST_TIME);

    boolean met = metBeamUnActivatedThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testMetBeamUnActivatedThresholdLessThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(false);
    when(smushyState.getBeamUnActivatedTime()).thenReturn(GLOBAL_REST_TIME - 1);

    boolean met = metBeamUnActivatedThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testMetBeamUnActivatedThresholdGreaterThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(false);
    when(smushyState.getBeamUnActivatedTime()).thenReturn(GLOBAL_REST_TIME + 3);

    boolean met = metBeamUnActivatedThreshold(smushyState);
    assertTrue(met);
  }

  @Test
  public void testMetBeamUnActivatedThresholdEquals() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(false);
    when(smushyState.getBeamUnActivatedTime()).thenReturn(GLOBAL_REST_TIME);

    boolean met = metBeamUnActivatedThreshold(smushyState);
    assertTrue(met);
  }
}
