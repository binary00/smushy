package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.GLOBAL_REST_TIME;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.metAlertInitialIndicatorThreshold;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_MetAlertInitialIndicatorThreshold {

  @Test
  public void testAlertInitialIndicatorThresholdNotIndicating() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertIndicateTime()).thenReturn(GLOBAL_REST_TIME );

    boolean met = metAlertInitialIndicatorThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testAlertInitialIndicatorThresholdLessThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(true);
    when(smushyState.getAlertIndicateTime()).thenReturn(GLOBAL_REST_TIME - 1);

    boolean met = metAlertInitialIndicatorThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testAlertInitialIndicatorThresholdGreaterThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(true);
    when(smushyState.getAlertIndicateTime()).thenReturn(GLOBAL_REST_TIME + 1);

    boolean met = metAlertInitialIndicatorThreshold(smushyState);
    assertTrue(met);
  }

  @Test
  public void testAlertInitialIndicatorThresholdEquals() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(true);
    when(smushyState.getAlertIndicateTime()).thenReturn(GLOBAL_REST_TIME);

    boolean met = metAlertInitialIndicatorThreshold(smushyState);
    assertTrue(met);
  }
}
