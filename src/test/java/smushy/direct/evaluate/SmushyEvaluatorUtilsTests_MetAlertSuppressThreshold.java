package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.GLOBAL_REST_TIME;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.metAlertSuppressThreshold;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_MetAlertSuppressThreshold {

  @Test
  public void testAlertSuppressThresholdNotSet() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);

    boolean met = metAlertSuppressThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testAlertSuppressThresholdIndicating() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(true);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME);

    boolean met = metAlertSuppressThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testAlertSuppressThresholdLessThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME - 1);

    boolean met = metAlertSuppressThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testAlertSuppressThresholdGreaterThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME + 4);

    boolean met = metAlertSuppressThreshold(smushyState);
    assertTrue(met);
  }

  @Test
  public void testAlertSuppressThresholdEquals() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME);

    boolean met = metAlertSuppressThreshold(smushyState);
    assertTrue(met);
  }
}
