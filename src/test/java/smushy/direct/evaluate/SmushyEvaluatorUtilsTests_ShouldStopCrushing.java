package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;
import smushy.sensor.distance.DistanceMovementType;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.*;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_ShouldStopCrushing {

  @Test
  public void testShouldStopCrushingFalse() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.isActuatorExtending()).thenReturn(true);
    when(smushyState.getDistanceMovementType()).thenReturn(DistanceMovementType.APPROACHING);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(MAX_CURRENT_THRESHOLD - 5);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE + 5);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isDoorClosed()).thenReturn(true);

    boolean stop = shouldStopCrushing(smushyState);
    assertFalse(stop);
  }

  @Test
  public void testShouldStopCrushingFailsCrushingVerification() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.isActuatorExtending()).thenReturn(true);
    when(smushyState.getDistanceMovementType()).thenReturn(DistanceMovementType.UNDETERMINED);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(MAX_CURRENT_THRESHOLD - 5);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE + 5);
    when(smushyState.isBeamActivated()).thenReturn(false);
    when(smushyState.isDoorClosed()).thenReturn(true);

    boolean stop = shouldStopCrushing(smushyState);
    assertTrue(stop);
  }

  @Test
  public void testShouldStopCrushingBeamUnactivated() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.isActuatorExtending()).thenReturn(true);
    when(smushyState.getDistanceMovementType()).thenReturn(DistanceMovementType.APPROACHING);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(MAX_CURRENT_THRESHOLD - 5);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE + 5);
    when(smushyState.isBeamActivated()).thenReturn(false);
    when(smushyState.isDoorClosed()).thenReturn(true);

    boolean stop = shouldStopCrushing(smushyState);
    assertTrue(stop);
  }

  @Test
  public void testShouldStopCrushingCurrentLimit() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.isActuatorExtending()).thenReturn(true);
    when(smushyState.getDistanceMovementType()).thenReturn(DistanceMovementType.APPROACHING);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(MAX_CURRENT_THRESHOLD + 5);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE + 1);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isDoorClosed()).thenReturn(true);

    boolean stop = shouldStopCrushing(smushyState);
    assertTrue(stop);
  }

  @Test
  public void testShouldStopCrushingMaxDistance() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.isActuatorExtending()).thenReturn(true);
    when(smushyState.getDistanceMovementType()).thenReturn(DistanceMovementType.APPROACHING);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(MAX_CURRENT_THRESHOLD - 5);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE - 1);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isDoorClosed()).thenReturn(true);

    boolean stop = shouldStopCrushing(smushyState);
    assertTrue(stop);
  }

  @Test
  public void testShouldStopCrushingDoorOpen() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.isActuatorExtending()).thenReturn(true);
    when(smushyState.getDistanceMovementType()).thenReturn(DistanceMovementType.APPROACHING);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(MAX_CURRENT_THRESHOLD - 5);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE + 5);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isDoorClosed()).thenReturn(false);

    boolean stop = shouldStopCrushing(smushyState);
    assertTrue(stop);
  }
}
