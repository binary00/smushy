package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.GLOBAL_REST_TIME;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.metActuatorIdleThreshold;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_MetActuatorIdleThreshold {

  @Test
  public void testActuatorIdleThresholdNotSet() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);

    boolean met = metActuatorIdleThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testActuatorIdleThresholdWithActivation() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 3);

    boolean met = metActuatorIdleThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testActuatorIdleThresholdLessThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME - 1);

    boolean met = metActuatorIdleThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testActuatorIdleThresholdGreaterThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 3);

    boolean met = metActuatorIdleThreshold(smushyState);
    assertTrue(met);
  }

  @Test
  public void testActuatorIdleThresholdEquals() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME);

    boolean met = metActuatorIdleThreshold(smushyState);
    assertTrue(met);
  }
}
