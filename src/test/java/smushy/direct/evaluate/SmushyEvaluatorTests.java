package smushy.direct.evaluate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import smushy.direct.state.ManualDirectState;
import smushy.direct.state.SmushyState;
import smushy.direct.state.SmushyStateBuilder;
import smushy.operate.actuator.ActuatorOperator;
import smushy.operate.indicator.AlertIndicator;
import smushy.sensor.beam.BeamSensor;
import smushy.sensor.current.CurrentSensor;
import smushy.sensor.distance.DistanceSensor;
import smushy.sensor.door.DoorSensor;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyStatic;
import static smushy.direct.SmushyAction.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SmushyEvaluatorUtils.class, SmushyStateBuilder.class})
public class SmushyEvaluatorTests {

  @InjectMocks
  private SmushyEvaluator smushyEvaluator;

  @Mock private DistanceSensor distanceSensor;
  @Mock private CurrentSensor currentSensor;
  @Mock private BeamSensor beamSensor;
  @Mock private DoorSensor doorSensor;
  @Mock private AlertIndicator alertIndicator;
  @Mock private ActuatorOperator actuatorOperator;
  @Mock private ManualDirectState manualDirectState;

  @Before
  public void setUp() throws Exception {
    mockStatic(SmushyEvaluatorUtils.class);
    mockStatic(SmushyStateBuilder.class);
  }

  @Test
  public void refreshWhenNotManuallyDirecting() throws Exception {
    when(SmushyEvaluatorUtils.areComponentsInitialized(any(), any(), any(), any(), any(), any())).thenReturn(true);
    when(manualDirectState.isDirecting()).thenReturn(false);
    SmushyState smushyState = mock(SmushyState.class);
    when(SmushyStateBuilder.createSmushyState(any(), any(), any(), any(), any(), any())).thenReturn(smushyState);
    when(SmushyEvaluatorUtils.shouldCrush(smushyState)).thenReturn(true);

    smushyEvaluator.refresh();

    verifyStatic(times(1));
    SmushyStateBuilder.createSmushyState(distanceSensor,
        currentSensor,
        beamSensor,
        doorSensor,
        alertIndicator,
        actuatorOperator);

    assertEquals(CRUSH, smushyEvaluator.getAction());
  }

  @Test
  public void refreshWhenManuallyDirecting() throws Exception {
    when(SmushyEvaluatorUtils.areComponentsInitialized(any(), any(), any(), any(), any(), any())).thenReturn(true);
    when(manualDirectState.isDirecting()).thenReturn(true);
    when(manualDirectState.getSmushyAction()).thenReturn(RETRACT);

    smushyEvaluator.refresh();

    assertEquals(RETRACT, smushyEvaluator.getAction());
  }

  @Test
  public void autoDirectWhenIndicate() throws Exception {
    when(SmushyEvaluatorUtils.areComponentsInitialized(any(), any(), any(), any(), any(), any())).thenReturn(true);
    when(manualDirectState.isDirecting()).thenReturn(false);
    SmushyState smushyState = mock(SmushyState.class);
    when(SmushyStateBuilder.createSmushyState(any(), any(), any(), any(), any(), any())).thenReturn(smushyState);
    when(SmushyEvaluatorUtils.shouldShowInitialIndicator(smushyState)).thenReturn(true);

    smushyEvaluator.refresh();

    assertEquals(INDICATE, smushyEvaluator.getAction());
  }

  @Test
  public void autoDirectWhenSuppressIndicate() throws Exception {
    when(SmushyEvaluatorUtils.areComponentsInitialized(any(), any(), any(), any(), any(), any())).thenReturn(true);
    when(manualDirectState.isDirecting()).thenReturn(false);
    SmushyState smushyState = mock(SmushyState.class);
    when(SmushyStateBuilder.createSmushyState(any(), any(), any(), any(), any(), any())).thenReturn(smushyState);
    when(SmushyEvaluatorUtils.shouldSuppressInitialIndicator(smushyState)).thenReturn(true);

    smushyEvaluator.refresh();

    assertEquals(RETRACT, smushyEvaluator.getAction());
  }

  @Test
  public void autoDirectWhenCrush() throws Exception {
    when(SmushyEvaluatorUtils.areComponentsInitialized(any(), any(), any(), any(), any(), any())).thenReturn(true);
    when(manualDirectState.isDirecting()).thenReturn(false);
    SmushyState smushyState = mock(SmushyState.class);
    when(SmushyStateBuilder.createSmushyState(any(), any(), any(), any(), any(), any())).thenReturn(smushyState);
    when(SmushyEvaluatorUtils.shouldCrush(smushyState)).thenReturn(true);

    smushyEvaluator.refresh();

    assertEquals(CRUSH, smushyEvaluator.getAction());
  }

  @Test
  public void autoDirectWhenStopCrushing() throws Exception {
    when(SmushyEvaluatorUtils.areComponentsInitialized(any(), any(), any(), any(), any(), any())).thenReturn(true);
    when(manualDirectState.isDirecting()).thenReturn(false);
    SmushyState smushyState = mock(SmushyState.class);
    when(SmushyStateBuilder.createSmushyState(any(), any(), any(), any(), any(), any())).thenReturn(smushyState);
    when(SmushyEvaluatorUtils.shouldStopCrushing(smushyState)).thenReturn(true);

    smushyEvaluator.refresh();

    assertEquals(RETRACT, smushyEvaluator.getAction());
  }

  @Test
  public void autoDirectWhenReset() throws Exception {
    when(SmushyEvaluatorUtils.areComponentsInitialized(any(), any(), any(), any(), any(), any())).thenReturn(true);
    when(manualDirectState.isDirecting()).thenReturn(false);
    SmushyState smushyState = mock(SmushyState.class);
    when(SmushyStateBuilder.createSmushyState(any(), any(), any(), any(), any(), any())).thenReturn(smushyState);
    when(SmushyEvaluatorUtils.shouldReset(smushyState)).thenReturn(true);

    smushyEvaluator.refresh();

    assertEquals(RESET, smushyEvaluator.getAction());
  }

  @Test
  public void autoDirectWhenNothing() throws Exception {
    when(SmushyEvaluatorUtils.areComponentsInitialized(any(), any(), any(), any(), any(), any())).thenReturn(true);
    when(manualDirectState.isDirecting()).thenReturn(false);
    SmushyState smushyState = mock(SmushyState.class);
    when(SmushyStateBuilder.createSmushyState(any(), any(), any(), any(), any(), any())).thenReturn(smushyState);

    smushyEvaluator.refresh();

    assertEquals(NOTHING, smushyEvaluator.getAction());
  }
}
