package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.CURRENT_LOAD_BUFFER_TIME;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.NO_LOAD_CURRENT;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.hasNoCurrentLoad;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_HasNoCurrentLoad {

  @Test
  public void testHasNoCurrentLoadNull() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(null);

    boolean load = hasNoCurrentLoad(smushyState);
    assertTrue(load);
  }

  @Test
  public void testHasNoCurrentLoadLessThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT - .5);

    boolean load = hasNoCurrentLoad(smushyState);
    assertTrue(load);
  }

  @Test
  public void testHasNoCurrentLoadGreaterThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT + 1);

    boolean load = hasNoCurrentLoad(smushyState);
    assertFalse(load);
  }

  @Test
  public void testHasNoCurrentLoadEquals() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);

    boolean load = hasNoCurrentLoad(smushyState);
    assertTrue(load);
  }

  @Test
  public void hasNoCurrentLoadWhenActuatorExtendingTimeNotMet() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuatorExtending()).thenReturn(true);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME - 1);

    boolean load = hasNoCurrentLoad(smushyState);
    assertFalse(load);
  }

  @Test
  public void hasNoCurrentLoadWhenExtending() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuatorExtending()).thenReturn(true);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean load = hasNoCurrentLoad(smushyState);
    assertTrue(load);
  }

  @Test
  public void hasNoCurrentLoadWhenActuatorRetractingTimeNotMet() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuatorRetracting()).thenReturn(true);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorRetractingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME - 1);

    boolean load = hasNoCurrentLoad(smushyState);
    assertFalse(load);
  }

  @Test
  public void hasNoCurrentLoadWhenRetracting() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuatorRetracting()).thenReturn(true);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorRetractingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean load = hasNoCurrentLoad(smushyState);
    assertTrue(load);
  }
}
