package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;
import smushy.sensor.distance.DistanceMovementType;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.CURRENT_LOAD_BUFFER_TIME;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.NO_LOAD_CURRENT;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.failsCrushingVerification;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_FailsCrushingVerification {

  @Test
  public void testFailsCrushingVerificationFalse() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);

    when(smushyState.getDistanceMovementType()).thenReturn(DistanceMovementType.APPROACHING);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT + 1);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean fails = failsCrushingVerification(smushyState);
    assertFalse(fails);
  }

  @Test
  public void testFailsCrushingVerificationNoLoadCurrent() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);

    when(smushyState.getDistanceMovementType()).thenReturn(DistanceMovementType.APPROACHING);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean fails = failsCrushingVerification(smushyState);
    assertTrue(fails);
  }

  @Test
  public void testFailsCrushingVerificationRecedingMovement() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);

    when(smushyState.getDistanceMovementType()).thenReturn(DistanceMovementType.RECEDING);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT + 1);
    when(smushyState.getActuatorExtendingTime()).thenReturn(CURRENT_LOAD_BUFFER_TIME);

    boolean fails = failsCrushingVerification(smushyState);
    assertTrue(fails);
  }
}
