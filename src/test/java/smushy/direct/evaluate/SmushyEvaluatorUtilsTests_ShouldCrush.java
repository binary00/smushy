package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.*;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_ShouldCrush {

  @Test
  public void testShouldCrushTrue() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME + 1);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean crush = shouldCrush(smushyState);
    assertTrue(crush);
  }

  @Test
  public void testShouldCrushNotMetBeamPreActuateThreshold() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME - 1);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean crush = shouldCrush(smushyState);
    assertFalse(crush);
  }

  @Test
  public void testShouldCrushNotMetActuatorIdleThreshold() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME + 5);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME - 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean crush = shouldCrush(smushyState);
    assertFalse(crush);
  }

  @Test
  public void testShouldCrushNotRetracted() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME + 5);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 5);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE - 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean crush = shouldCrush(smushyState);
    assertFalse(crush);
  }

  @Test
  public void testShouldCrushDoorOpen() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME + 1);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(false);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean crush = shouldCrush(smushyState);
    assertFalse(crush);
  }

  @Test
  public void testShouldCrushWhenPostCrushDoorOpenCheckFail() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME + 1);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(3);
    when(smushyState.getDoorLastOpenTime()).thenReturn(5);

    boolean crush = shouldCrush(smushyState);
    assertFalse(crush);
  }
}
