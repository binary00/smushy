package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.*;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_ShouldReset {

  @Test
  public void shouldResetWhenWaitTimeMetAndFullyRetracted() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);

    assertFalse(shouldReset(smushyState));
  }

  @Test
  public void shouldResetWhenWaitTimeNotMetAndFullyRetracted() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME - 1);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);

    assertFalse(shouldReset(smushyState));
  }

  @Test
  public void shouldResetWhenWaitTimeMetAndNotFullyRetractedDistance() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE - 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);

    assertTrue(shouldReset(smushyState));
  }

  @Test
  public void shouldResetWhenWaitTimeMetAndNotFullyRetractedCurrent() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT + 1);

    assertTrue(shouldReset(smushyState));
  }
}
