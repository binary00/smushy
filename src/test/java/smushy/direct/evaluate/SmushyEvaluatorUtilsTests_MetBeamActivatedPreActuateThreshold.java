package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.BEAM_PRE_ACTUATE_REST_TIME;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.metBeamActivatedPreActuateThreshold;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_MetBeamActivatedPreActuateThreshold {

  @Test
  public void testMetBeamActivatedPreActuateThresholdNotSet() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);

    boolean met = metBeamActivatedPreActuateThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testMetBeamActivatedPreActuateThresholdNotActivated() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(false);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME + 5);

    boolean met = metBeamActivatedPreActuateThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testMetBeamActivatedPreActuateThresholdLessThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME - 1);

    boolean met = metBeamActivatedPreActuateThreshold(smushyState);
    assertFalse(met);
  }

  @Test
  public void testMetBeamActivatedPreActuateThresholdGreaterThan() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME + 5);

    boolean met = metBeamActivatedPreActuateThreshold(smushyState);
    assertTrue(met);
  }

  @Test
  public void testMetBeamActivatedPreActuateThresholdEquals() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.getBeamActivatedTime()).thenReturn(BEAM_PRE_ACTUATE_REST_TIME);

    boolean met = metBeamActivatedPreActuateThreshold(smushyState);
    assertTrue(met);
  }
}
