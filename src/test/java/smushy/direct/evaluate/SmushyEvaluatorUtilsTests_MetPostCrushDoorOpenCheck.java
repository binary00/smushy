package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.NO_LOAD_CURRENT;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.RETRACTED_DISTANCE;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.metPostCrushDoorOpenCheck;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_MetPostCrushDoorOpenCheck {

  @Test
  public void metPostCrushDoorOpenCheckWhenActuatorAfterDoorOpen() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorIdleTime()).thenReturn(3);
    when(smushyState.getDoorLastOpenTime()).thenReturn(5);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);

    assertFalse(metPostCrushDoorOpenCheck(smushyState));
  }

  @Test
  public void metPostCrushDoorOpenCheckWhenActuatorBeforeDoorOpen() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorIdleTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);

    assertTrue(metPostCrushDoorOpenCheck(smushyState));
  }

  @Test
  public void metPostCrushDoorOpenCheckWhenActuatorSameAsDoorOpen() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.getActuatorIdleTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(5);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);

    assertTrue(metPostCrushDoorOpenCheck(smushyState));
  }
}
