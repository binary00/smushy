package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.MAX_EXTENDED_DISTANCE;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.isActuatorAtMaxExtension;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_IsActuatorAtMaxExtension {

  @Test
  public void testActuatorAtMaxExtensionNullDistance() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.getActuatorDistance()).thenReturn(null);

    boolean max = isActuatorAtMaxExtension(smushyState);
    assertFalse(max);
  }

  @Test
  public void testActuatorAtMaxExtensionNotActuating() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE - 1);

    boolean max = isActuatorAtMaxExtension(smushyState);
    assertFalse(max);
  }

  @Test
  public void testActuatorAtMaxExtensionFalse() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE + .5);

    boolean max = isActuatorAtMaxExtension(smushyState);
    assertFalse(max);
  }

  @Test
  public void testActuatorAtMaxExtensionTrue() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE - .5);

    boolean max = isActuatorAtMaxExtension(smushyState);
    assertTrue(max);
  }

  @Test
  public void testActuatorAtMaxExtensionEquals() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isActuating()).thenReturn(true);
    when(smushyState.getActuatorDistance()).thenReturn(MAX_EXTENDED_DISTANCE);

    boolean max = isActuatorAtMaxExtension(smushyState);
    assertFalse(max);
  }
}
