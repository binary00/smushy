package smushy.direct.evaluate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.direct.state.SmushyState;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.*;

@RunWith(MockitoJUnitRunner.class)
public class SmushyEvaluatorUtilsTests_ShouldShowInitialIndicator {

  @Test
  public void testShouldShowInitialIndicatorTrue() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean indicate = shouldShowInitialIndicator(smushyState);
    assertTrue(indicate);
  }

  @Test
  public void testShouldShowInitialIndicatorNotMetBeamUnactivatedThreshold() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME - 1);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean indicate = shouldShowInitialIndicator(smushyState);
    assertFalse(indicate);
  }

  @Test
  public void testShouldShowInitialIndicatorNotMetAlertSuppressThreshold() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME - 1);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean indicate = shouldShowInitialIndicator(smushyState);
    assertFalse(indicate);
  }

  @Test
  public void testShouldShowInitialIndicatorNotMetActuatorIdleThreshold() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME - 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean indicate = shouldShowInitialIndicator(smushyState);
    assertFalse(indicate);
  }

  @Test
  public void testShouldShowInitialIndicatorNotRetracted() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE - 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(5);
    when(smushyState.getDoorLastOpenTime()).thenReturn(3);

    boolean indicate = shouldShowInitialIndicator(smushyState);
    assertFalse(indicate);
  }

  @Test
  public void testShouldShowInitialIndicatorPostCrushDoorOpenCheckFail() throws Exception {
    SmushyState smushyState = mock(SmushyState.class);
    when(smushyState.isAlertIndicating()).thenReturn(false);
    when(smushyState.getAlertSuppressTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.isBeamActivated()).thenReturn(true);
    when(smushyState.isActuating()).thenReturn(false);
    when(smushyState.getActuatorIdleTime()).thenReturn(GLOBAL_REST_TIME + 1);
    when(smushyState.getActuatorCurrentUsage()).thenReturn(NO_LOAD_CURRENT);
    when(smushyState.getActuatorDistance()).thenReturn(RETRACTED_DISTANCE + 1);
    when(smushyState.isDoorClosed()).thenReturn(true);
    when(smushyState.getActuatorLastRetractTime()).thenReturn(3);
    when(smushyState.getDoorLastOpenTime()).thenReturn(5);

    boolean indicate = shouldShowInitialIndicator(smushyState);
    assertFalse(indicate);
  }
}
