package smushy.direct;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import smushy.direct.evaluate.SmushyEvaluator;
import smushy.direct.state.OverallState;
import smushy.operate.actuator.ActuatorOperator;
import smushy.operate.indicator.AlertIndicator;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SmushyDirectorTests {

  @InjectMocks
  private SmushyDirector smushyDirector = spy(new SmushyDirector());

  @Mock private AlertIndicator alertIndicator;
  @Mock private ActuatorOperator actuatorOperator;
  @Mock private SimpMessagingTemplate messagingTemplate;
  @Mock private SmushyEvaluator smushyEvaluator;

  @Test
  public void testRefresh() throws Exception {
    SmushyAction action = SmushyAction.CRUSH;
    when(smushyEvaluator.getAction()).thenReturn(action);
    OverallState overallState = mock(OverallState.class);
    when(smushyEvaluator.overallState()).thenReturn(overallState);

    smushyDirector.refresh();

    verify(smushyDirector, times(1)).direct(action);
    verify(messagingTemplate, times(1)).convertAndSend("/topic/sensor-state-changes", overallState);
  }

  @Test
  public void testDirectIndicate() throws Exception {
    smushyDirector.direct(SmushyAction.INDICATE);

    verify(alertIndicator, times(1)).indicate();
  }

  @Test
  public void testDirectCrush() throws Exception {
    smushyDirector.direct(SmushyAction.CRUSH);

    verify(alertIndicator, times(1)).indicate();
    verify(actuatorOperator, times(1)).extend();
  }

  @Test
  public void testDirectRetract() throws Exception {
    smushyDirector.direct(SmushyAction.RETRACT);

    verify(alertIndicator, times(1)).suppress();
    verify(actuatorOperator, times(1)).retract();
  }

  @Test
  public void testDirectStop() throws Exception {
    smushyDirector.direct(SmushyAction.STOP);

    verify(alertIndicator, times(1)).suppress();
    verify(actuatorOperator, times(1)).stop();
  }

  @Test
  public void testDirectReset() throws Exception {
    smushyDirector.direct(SmushyAction.RESET);

    verify(alertIndicator, times(1)).suppress();
    verify(actuatorOperator, times(1)).retract();
  }

  @Test
  public void testDirectNothing() throws Exception {
    smushyDirector.direct(SmushyAction.NOTHING);

    verify(alertIndicator, never()).suppress();
    verify(alertIndicator, never()).indicate();
    verify(actuatorOperator, never()).retract();
    verify(actuatorOperator, never()).stop();
    verify(actuatorOperator, never()).extend();
  }
}
