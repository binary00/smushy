package smushy.direct.state;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import smushy.operate.actuator.ActuatorOperator;
import smushy.operate.indicator.AlertIndicator;
import smushy.sensor.beam.BeamSensor;
import smushy.sensor.current.CurrentSensor;
import smushy.sensor.distance.DistanceMovementType;
import smushy.sensor.distance.DistanceSensor;
import smushy.sensor.door.DoorSensor;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SmushyStateBuilderTests {

  @Test
  public void testCreateSmushyState() throws Exception {
    // Using slightly unrealistic values to make sure we're not testing defaults
    // booleans returning false..etc.

    DistanceSensor distanceSensor = mock(DistanceSensor.class);
    Double distance = 25D;
    when(distanceSensor.getDistance()).thenReturn(distance);
    DistanceMovementType movementType = DistanceMovementType.APPROACHING;
    when(distanceSensor.getMovementType()).thenReturn(movementType);

    CurrentSensor currentSensor = mock(CurrentSensor.class);
    Double current = 1D;
    when(currentSensor.getCurrent()).thenReturn(current);

    BeamSensor beamSensor = mock(BeamSensor.class);
    boolean beamActivated = true;
    when(beamSensor.isActivated()).thenReturn(beamActivated);
    int beamActivatedTime = 3;
    when(beamSensor.getElapsedActivatedTime()).thenReturn(beamActivatedTime);
    int beamUnActivatedTime = 2;
    when(beamSensor.getElapsedUnActivatedTime()).thenReturn(beamUnActivatedTime);

    DoorSensor doorSensor = mock(DoorSensor.class);
    boolean doorActivated = true;
    when(doorSensor.isActivated()).thenReturn(doorActivated);
    int doorClosedTime = 3;
    when(doorSensor.getElapsedUnActivatedTime()).thenReturn(doorClosedTime);
    int doorOpenTime = 2;
    when(doorSensor.getElapsedActivatedTime()).thenReturn(doorOpenTime);
    int doorLastOpenTime = 5;
    when(doorSensor.getDoorLastOpenTime()).thenReturn(doorLastOpenTime);

    AlertIndicator alertIndicator = mock(AlertIndicator.class);
    boolean alertIndicating = true;
    when(alertIndicator.isIndicating()).thenReturn(alertIndicating);
    int alertIndicatingTime = 1;
    when(alertIndicator.getElapsedIndicateTime()).thenReturn(alertIndicatingTime);
    int alertSuppressTime = 7;
    when(alertIndicator.getElapsedSuppressTime()).thenReturn(alertSuppressTime);

    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    boolean isActuating = true;
    when(actuatorOperator.isActuating()).thenReturn(isActuating);
    boolean isActuatorExtending = true;
    when(actuatorOperator.isExtending()).thenReturn(isActuatorExtending);
    boolean isActuatorRetracting = true;
    when(actuatorOperator.isRetracting()).thenReturn(isActuatorRetracting);
    int actuateExtendingTime = 6;
    when(actuatorOperator.getElapsedExtendingTime()).thenReturn(actuateExtendingTime);
    int actuateRetractTime = 5;
    when(actuatorOperator.getElapsedRetractingTime()).thenReturn(actuateRetractTime);
    int actuateIdleTime = 3;
    when(actuatorOperator.getElapsedIdleTime()).thenReturn(actuateIdleTime);
    int actuatorLastRetractTime = 9;
    when(actuatorOperator.getLastRetractedTime()).thenReturn(actuatorLastRetractTime);

    long refreshTime = System.currentTimeMillis();

    SmushyState smushyState = SmushyStateBuilder.createSmushyState(distanceSensor,
                                                currentSensor,
                                                beamSensor,
                                                doorSensor,
                                                alertIndicator,
                                                actuatorOperator);


    assertNotNull(smushyState);
    assertEquals(distance, smushyState.getActuatorDistance(), 0);
    assertEquals(movementType, smushyState.getDistanceMovementType());
    assertEquals(current, smushyState.getActuatorCurrentUsage(), 0);
    assertEquals(beamActivated, smushyState.isBeamActivated());
    assertEquals(beamActivatedTime, smushyState.getBeamActivatedTime());
    assertEquals(beamUnActivatedTime, smushyState.getBeamUnActivatedTime());
    assertEquals(!doorActivated, smushyState.isDoorClosed());
    assertEquals(doorClosedTime, smushyState.getDoorClosedTime());
    assertEquals(doorOpenTime, smushyState.getDoorOpenTime());
    assertEquals(doorLastOpenTime, smushyState.getDoorLastOpenTime());
    assertEquals(alertIndicating, smushyState.isAlertIndicating());
    assertEquals(alertIndicatingTime, smushyState.getAlertIndicateTime());
    assertEquals(alertSuppressTime, smushyState.getAlertSuppressTime());
    assertEquals(isActuating, smushyState.isActuating());
    assertEquals(isActuatorExtending, smushyState.isActuatorExtending());
    assertEquals(isActuatorRetracting, smushyState.isActuatorRetracting());
    assertEquals(actuateExtendingTime, smushyState.getActuatorExtendingTime());
    assertEquals(actuateRetractTime, smushyState.getActuatorRetractingTime());
    assertEquals(actuateIdleTime, smushyState.getActuatorIdleTime());
    assertEquals(actuatorLastRetractTime, smushyState.getActuatorLastRetractTime());
    assertTrue(refreshTime <= smushyState.getRefreshDate());
  }

  @Test
  public void testCreateSmushyStateNulls() throws Exception {

    long refreshTime = System.currentTimeMillis();
    SmushyState smushyState =
        SmushyStateBuilder.createSmushyState(null, null, null, null, null, null);

    assertNotNull(smushyState);
    assertTrue(refreshTime <= smushyState.getRefreshDate());
  }

}
