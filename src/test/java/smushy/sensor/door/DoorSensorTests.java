package smushy.sensor.door;

import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DoorSensorTests {

  @Mock
  private GpioPinDigitalInput input;

  @Mock
  private GpioPinDigitalStateChangeEvent event;

  @Test
  public void testDoorSensorLowState() throws Exception {
    DoorSensor doorSensor = spy(new DoorSensor(input));
    GpioPinDigitalStateChangeEvent event = mock(GpioPinDigitalStateChangeEvent.class);

    boolean activated = doorSensor.isActivated();
    assertFalse(activated);

    when(event.getState()).thenReturn(PinState.LOW);

    doorSensor.handleCallback(event);
    activated = doorSensor.isActivated();
    assertTrue(activated);

    Thread.sleep(1100);

    int activeTime = doorSensor.getElapsedActivatedTime();
    assertTrue(activeTime >= 1);

    int unActiveTime = doorSensor.getElapsedUnActivatedTime();
    assertEquals(0, unActiveTime);
  }

  @Test
  public void testDoorSensorHighState() throws Exception {
    DoorSensor doorSensor = spy(new DoorSensor(input));
    GpioPinDigitalStateChangeEvent event = mock(GpioPinDigitalStateChangeEvent.class);

    boolean activated = doorSensor.isActivated();
    assertFalse(activated);

    when(event.getState()).thenReturn(PinState.HIGH);

    doorSensor.handleCallback(event);
    activated = doorSensor.isActivated();
    assertFalse(activated);

    Thread.sleep(1100);

    int unActiveTime = doorSensor.getElapsedUnActivatedTime();
    assertTrue(unActiveTime >= 1);

    int activeTime = doorSensor.getElapsedActivatedTime();
    assertEquals(0, activeTime);
  }

  @Test
  public void testDoorSensorStartUpTimer() throws Exception {
    DoorSensor doorSensor = spy(new DoorSensor(input));

    Thread.sleep(1100);

    int unActiveTime = doorSensor.getElapsedUnActivatedTime();
    assertTrue(unActiveTime >= 1);
  }

}
