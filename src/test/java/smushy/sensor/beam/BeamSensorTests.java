package smushy.sensor.beam;

import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BeamSensorTests {

  @Mock
  private GpioPinDigitalInput input;

  @Mock
  private GpioPinDigitalStateChangeEvent event;

  @Test
  public void testBeamSensorLowState() throws Exception {
    BeamSensor beamSensor = spy(new BeamSensor(input));
    GpioPinDigitalStateChangeEvent event = mock(GpioPinDigitalStateChangeEvent.class);

    boolean activated = beamSensor.isActivated();
    assertFalse(activated);

    when(event.getState()).thenReturn(PinState.LOW);

    beamSensor.handleCallback(event);
    activated = beamSensor.isActivated();
    assertTrue(activated);

    Thread.sleep(1100);

    int activeTime = beamSensor.getElapsedActivatedTime();
    assertTrue(activeTime >= 1);

    int unActiveTime = beamSensor.getElapsedUnActivatedTime();
    assertEquals(0, unActiveTime);
  }

  @Test
  public void testBeamSensorHighState() throws Exception {
    BeamSensor beamSensor = spy(new BeamSensor(input));
    GpioPinDigitalStateChangeEvent event = mock(GpioPinDigitalStateChangeEvent.class);

    boolean activated = beamSensor.isActivated();
    assertFalse(activated);

    when(event.getState()).thenReturn(PinState.HIGH);

    beamSensor.handleCallback(event);
    activated = beamSensor.isActivated();
    assertFalse(activated);

    Thread.sleep(1100);

    int unActiveTime = beamSensor.getElapsedUnActivatedTime();
    assertTrue(unActiveTime >= 1);

    int activeTime = beamSensor.getElapsedActivatedTime();
    assertEquals(0, activeTime);
  }

  @Test
  public void testBeamSensorStartUpTimer() throws Exception {
    BeamSensor beamSensor = spy(new BeamSensor(input));

    Thread.sleep(1100);

    int unActiveTime = beamSensor.getElapsedUnActivatedTime();
    assertTrue(unActiveTime >= 1);
  }
}
