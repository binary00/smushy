package smushy.sensor.distance;


import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Queues;
import org.junit.Test;

import java.util.Queue;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static smushy.sensor.distance.DistanceUtils.*;

public class DistanceUtilsTest {

  @Test
  public void testMovementTypeNullDistances() throws Exception {
    DistanceMovementType movementType = determineMovementType(null);
    assertEquals(DistanceMovementType.UNDETERMINED, movementType);
  }

  @Test
  public void testMovementTypeNotEnoughDistances() throws Exception {
    Queue<Double> distanceHistory = createSyncQueue(2);
    distanceHistory.add(1D);
    DistanceMovementType movementType = determineMovementType(distanceHistory);
    assertEquals(DistanceMovementType.UNDETERMINED, movementType);
  }

  @Test
  public void testMovementTypeEmpty() throws Exception {
    Queue<Double> distanceHistory = createSyncQueue(2);
    DistanceMovementType movementType = determineMovementType(distanceHistory);
    assertEquals(DistanceMovementType.UNDETERMINED, movementType);
  }

  @Test
  public void testMovementTypeReceding() throws Exception {
    Queue<Double> distanceHistory = createSyncQueue(5);
    distanceHistory.add(5D);
    distanceHistory.add(7D);
    distanceHistory.add(9D);
    distanceHistory.add(11D);
    distanceHistory.add(13D);
    DistanceMovementType movementType = determineMovementType(distanceHistory);
    assertEquals(DistanceMovementType.RECEDING, movementType);
  }

  @Test
  public void testMovementTypeApproaching() throws Exception {
    Queue<Double> distanceHistory = createSyncQueue(5);
    distanceHistory.add(13D);
    distanceHistory.add(5D);
    distanceHistory.add(7D);
    distanceHistory.add(9D);
    distanceHistory.add(11D);
    DistanceMovementType movementType = determineMovementType(distanceHistory);
    assertEquals(DistanceMovementType.APPROACHING, movementType);
  }

  @Test
  public void testMovementTypeStopped() throws Exception {
    Queue<Double> distanceHistory = createSyncQueue(5);
    distanceHistory.add(0D);
    distanceHistory.add(0D);
    distanceHistory.add(0D);
    distanceHistory.add(0D);
    distanceHistory.add(0D);
    DistanceMovementType movementType = determineMovementType(distanceHistory);
    assertEquals(DistanceMovementType.STOPPED, movementType);
  }

  @Test
  public void testInvertDistance() throws Exception {
    Double distance = invertDistance(14D);
    assertEquals((INVERT_DISTANCE - 14D), 0, distance);
  }

  @Test
  public void testInvertDistanceNull() throws Exception {
    Double distance = invertDistance(null);
    assertNull(distance);
  }

  @Test
  public void testInvertDistanceGreaterThanInvertMax() throws Exception {
    Double distance = invertDistance(INVERT_DISTANCE + 5);
    assertEquals(ZERO, distance);
  }

  private Queue<Double> createSyncQueue(int size) {
    return Queues.synchronizedQueue(EvictingQueue.create(size));
  }
}
