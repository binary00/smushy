package smushy.sensor.distance.tof;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TofDistanceSensorUtilsTest {

  @Test
  public void testCMToInchesConversion() throws Exception {

    Double converted = TofDistanceSensorUtils.convertMMToInches(30);
    assertEquals(1.181103D, converted, 0);

  }
}
