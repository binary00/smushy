package smushy.sensor.current;

import com.pi4j.gpio.extension.ads.ADS1115GpioProvider;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.GpioPinAnalogInput;
import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListener;
import org.junit.Test;
import smushy.operate.actuator.ActuatorOperator;

import static com.pi4j.gpio.extension.ads.ADS1x15GpioProvider.ProgrammableGainAmplifierValue.PGA_4_096V;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static smushy.sensor.current.CurrentSensorType.EXTENDING;

public class CurrentSensorTests {

  @Test
  public void testCurrentSensorWiringInit() throws Exception {
    GpioPinAnalogInput extendingSensor = mock(GpioPinAnalogInput.class);
    GpioPinAnalogInput retractingSensor = mock(GpioPinAnalogInput.class);
    ADS1115GpioProvider provider = mock(ADS1115GpioProvider.class);
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);

    new CurrentSensor(extendingSensor, retractingSensor, provider, actuatorOperator);

    verify(extendingSensor, times(1)).addListener(any(GpioPinListener.class));
    verify(retractingSensor, times(1)).addListener(any(GpioPinListener.class));
  }

  @Test
  public void handleCurrent() throws Exception {
    GpioPinAnalogInput extendingSensor = mock(GpioPinAnalogInput.class);
    GpioPinAnalogInput retractingSensor = mock(GpioPinAnalogInput.class);
    ADS1115GpioProvider provider = mock(ADS1115GpioProvider.class);
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    when(actuatorOperator.isExtending()).thenReturn(true);

    GpioPin pin = mock(GpioPin.class);
    GpioPinAnalogValueChangeEvent event = mock(GpioPinAnalogValueChangeEvent.class);
    when(event.getPin()).thenReturn(pin);
    when(event.getValue()).thenReturn(24000D);

    when(provider.getProgrammableGainAmplifier(pin)).thenReturn(PGA_4_096V);

    CurrentSensor currentSensor = new CurrentSensor(extendingSensor, retractingSensor, provider, actuatorOperator);
    currentSensor.handleCurrent(event, EXTENDING);

    Double current = currentSensor.getCurrent();
    assertEquals(6.772195973221395D, current, 0);
  }
}
