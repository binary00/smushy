package smushy.sensor.current;


import com.pi4j.gpio.extension.ads.ADS1115GpioProvider;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import org.junit.Test;
import smushy.operate.actuator.ActuatorOperator;

import static com.pi4j.gpio.extension.ads.ADS1x15GpioProvider.ProgrammableGainAmplifierValue.PGA_4_096V;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static smushy.sensor.current.CurrentSensorType.EXTENDING;
import static smushy.sensor.current.CurrentSensorType.RETRACTING;
import static smushy.sensor.current.CurrentSensorUtils.*;

public class CurrentSensorUtilsTests {

  @Test
  public void testShouldCalculateRetractingButNotType() throws Exception {
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    when(actuatorOperator.isRetracting()).thenReturn(true);

    boolean calculate = shouldCalculate(actuatorOperator, EXTENDING);
    assertFalse(calculate);
  }

  @Test
  public void testShouldCalculateNotRetracting() throws Exception {
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    when(actuatorOperator.isRetracting()).thenReturn(false);

    boolean calculate = shouldCalculate(actuatorOperator, RETRACTING);
    assertFalse(calculate);
  }

  @Test
  public void testShouldCalculateRetracting() throws Exception {
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    when(actuatorOperator.isRetracting()).thenReturn(true);

    boolean calculate = shouldCalculate(actuatorOperator, RETRACTING);
    assertTrue(calculate);
  }

  @Test
  public void testShouldCalculateExtendingButNotType() throws Exception {
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    when(actuatorOperator.isExtending()).thenReturn(true);

    boolean calculate = shouldCalculate(actuatorOperator, RETRACTING);
    assertFalse(calculate);
  }

  @Test
  public void testShouldCalculateNotExtending() throws Exception {
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    when(actuatorOperator.isExtending()).thenReturn(false);

    boolean calculate = shouldCalculate(actuatorOperator, EXTENDING);
    assertFalse(calculate);
  }

  @Test
  public void testShouldCalculateExtending() throws Exception {
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    when(actuatorOperator.isExtending()).thenReturn(true);

    boolean calculate = shouldCalculate(actuatorOperator, EXTENDING);
    assertTrue(calculate);
  }

  @Test
  public void testShouldCalculateNull() throws Exception {
    boolean calculate = shouldCalculate(null, null);
    assertFalse(calculate);
  }

  @Test
  public void testCalculateCurrentValueResultsInNonNegative() throws Exception {
    GpioPin pin = mock(GpioPin.class);
    GpioPinAnalogValueChangeEvent event = mock(GpioPinAnalogValueChangeEvent.class);
    when(event.getPin()).thenReturn(pin);

    ADS1115GpioProvider provider = mock(ADS1115GpioProvider.class);
    when(provider.getProgrammableGainAmplifier(pin)).thenReturn(PGA_4_096V);

    when(event.getValue()).thenReturn(24000D);

    Double current = calculateCurrent(event, provider);
    assertEquals(6.772195973221395D, current, 0);
  }

  @Test
  public void testCalculateCurrentValueResultsInNegative() throws Exception {
    GpioPin pin = mock(GpioPin.class);
    GpioPinAnalogValueChangeEvent event = mock(GpioPinAnalogValueChangeEvent.class);
    when(event.getPin()).thenReturn(pin);

    ADS1115GpioProvider provider = mock(ADS1115GpioProvider.class);
    when(provider.getProgrammableGainAmplifier(pin)).thenReturn(PGA_4_096V);

    when(event.getValue()).thenReturn(100D);

    Double current = calculateCurrent(event, provider);
    assertEquals(0, current, 0);
  }

  @Test
  public void testCalculateCurrentValueIsNull() throws Exception {
    GpioPin pin = mock(GpioPin.class);
    GpioPinAnalogValueChangeEvent event = mock(GpioPinAnalogValueChangeEvent.class);
    when(event.getPin()).thenReturn(pin);

    ADS1115GpioProvider provider = mock(ADS1115GpioProvider.class);
    when(provider.getProgrammableGainAmplifier(pin)).thenReturn(PGA_4_096V);

    Double current = calculateCurrent(event, provider);
    assertEquals(0, current, 0);
  }

  @Test
  public void testDetermineCurrentNulls() throws Exception {
    Double current = determineCurrent(null, null, null);
    assertEquals(DEFAULT_CURRENT, current, 0);
  }

  @Test
  public void testDetermineCurrentIsActuating() throws Exception {
    Double lastKnownCurrent = 5D;
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    when(actuatorOperator.isExtending()).thenReturn(true);

    Double current = determineCurrent(lastKnownCurrent, actuatorOperator, EXTENDING);
    assertEquals(lastKnownCurrent, current, 0);
  }

  @Test
  public void testDetermineCurrentIsNotActuating() throws Exception {
    Double lastKnownCurrent = 5D;
    ActuatorOperator actuatorOperator = mock(ActuatorOperator.class);
    when(actuatorOperator.isExtending()).thenReturn(false);

    Double current = determineCurrent(lastKnownCurrent, actuatorOperator, EXTENDING);
    assertEquals(DEFAULT_CURRENT, current, 0);
  }
}
