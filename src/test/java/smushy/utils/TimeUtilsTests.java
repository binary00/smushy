package smushy.utils;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static smushy.utils.TimeUtils.calculateElapsedTimeInSeconds;

public class TimeUtilsTests {

  @Test
  public void testCalculateTime() throws Exception {
    long startTime = System.currentTimeMillis();
    Thread.sleep(2000);

    int timeInSeconds = calculateElapsedTimeInSeconds(startTime);
    assertEquals(1, String.valueOf(timeInSeconds).length());
    assertTrue(2 <= timeInSeconds);
  }

  @Test
  public void testCalculateTimeInvalid() throws Exception {
    int timeInSeconds = calculateElapsedTimeInSeconds(-1);
    assertEquals(1, String.valueOf(timeInSeconds).length());
    assertEquals(0, timeInSeconds);
  }

  @Test
  public void testCalculateTimeLessThanSecond() throws Exception {
    long startTime = System.currentTimeMillis();
    Thread.sleep(500);

    int timeInSeconds = calculateElapsedTimeInSeconds(startTime);
    assertEquals(1, String.valueOf(timeInSeconds).length());
    assertEquals(0, timeInSeconds);
  }

  @Test
  public void testPartialSecond() throws Exception {
    long startTime = System.currentTimeMillis();
    Thread.sleep(1700);

    int timeInSeconds = calculateElapsedTimeInSeconds(startTime);
    assertEquals(1, timeInSeconds);
  }

  @Test
  public void maxValueReturnedWhenTimeGreaterThanInt() throws Exception {
    int timeInSeconds = calculateElapsedTimeInSeconds(1);
    assertEquals(Integer.MAX_VALUE, timeInSeconds);
  }
}
