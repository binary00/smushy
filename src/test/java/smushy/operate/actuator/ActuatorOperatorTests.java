package smushy.operate.actuator;


import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static smushy.operate.actuator.ActuatorOperator.STOP_WAIT;

public class ActuatorOperatorTests {

  private GpioPinDigitalOutput positiveControl;
  private GpioPinDigitalOutput negativeControl;
  private GpioPinDigitalOutput relayPower;
  private ActuatorOperator actuatorOperator;

  @Before
  public void setUp() throws Exception {
    positiveControl = mock(GpioPinDigitalOutput.class);
    negativeControl = mock(GpioPinDigitalOutput.class);
    relayPower = mock(GpioPinDigitalOutput.class);
    actuatorOperator = new ActuatorOperator(positiveControl, negativeControl, relayPower);
  }

  @Test
  public void testCreationIdleTime() throws Exception {
    Thread.sleep(1005);
    assertTrue(actuatorOperator.getElapsedIdleTime() >= 1);
  }

  @Test
  public void testIdleTimingAfterCycle() throws Exception {
    ScheduledExecutorService extendingThread = async(() -> actuatorOperator.extend(), 5);
    Thread.sleep(15);

    ScheduledExecutorService retractingThread = async(() -> actuatorOperator.retract(), 5);
    Thread.sleep(STOP_WAIT + 20);

    actuatorOperator.stop();
    Thread.sleep(1500);

    int elapsedIdleTime = actuatorOperator.getElapsedIdleTime();
    assertEquals(1, elapsedIdleTime);

    extendingThread.shutdownNow();
    retractingThread.shutdownNow();
  }

  @Test
  public void testExtendTiming() throws Exception {
    ScheduledExecutorService extendingThread = async(() -> actuatorOperator.extend(), 5);
    Thread.sleep(1100);

    int elapsedExtendingTime = actuatorOperator.getElapsedExtendingTime();
    assertEquals(1, elapsedExtendingTime);

    extendingThread.shutdownNow();
  }

  @Test
  public void testRetractTiming() throws Exception {
    ScheduledExecutorService extendingThread = async(() -> actuatorOperator.extend(), 5);
    Thread.sleep(200);

    ScheduledExecutorService retractingThread = async(() -> actuatorOperator.retract(), 5);
    Thread.sleep(1500);

    int elapsedExtendingTime = actuatorOperator.getElapsedRetractingTime();
    assertEquals(1, elapsedExtendingTime);

    extendingThread.shutdownNow();
    retractingThread.shutdownNow();
  }

  @Test
  public void testExtendAndRetractBooleans() throws Exception {
    assertFalse(actuatorOperator.isExtending());
    assertFalse(actuatorOperator.isRetracting());
    assertFalse(actuatorOperator.isActuating());

    ScheduledExecutorService extendingThread = async(() -> actuatorOperator.extend(), 5);
    Thread.sleep(15);

    assertTrue(actuatorOperator.isActuating());
    assertTrue(actuatorOperator.isExtending());
    assertFalse(actuatorOperator.isRetracting());

    ScheduledExecutorService retractingThread = async(() -> actuatorOperator.retract(), 5);
    Thread.sleep(STOP_WAIT + 20);

    assertTrue(actuatorOperator.isActuating());
    assertTrue(actuatorOperator.isRetracting());
    assertFalse(actuatorOperator.isExtending());

    actuatorOperator.stop();
    Thread.sleep(STOP_WAIT + 20);

    assertFalse(actuatorOperator.isActuating());
    assertFalse(actuatorOperator.isRetracting());
    assertFalse(actuatorOperator.isExtending());

    extendingThread.shutdownNow();
    retractingThread.shutdownNow();
  }

  @Test
  public void testExtendStop() throws Exception {
    ScheduledExecutorService extendingThread = async(() -> actuatorOperator.extend(), 5);
    Thread.sleep(15);

    assertTrue(actuatorOperator.isExtending());

    actuatorOperator.stop();
    Thread.sleep(STOP_WAIT + 20);

    assertFalse(actuatorOperator.isExtending());

    extendingThread.shutdownNow();
  }

  @Test
  public void testRetractStop() throws Exception {
    ScheduledExecutorService retractingThread = async(() -> actuatorOperator.retract(), 5);
    Thread.sleep(15);

    assertTrue(actuatorOperator.isRetracting());

    actuatorOperator.stop();
    Thread.sleep(STOP_WAIT + 20);

    assertFalse(actuatorOperator.isRetracting());

    retractingThread.shutdownNow();
  }

  @Test
  public void testExtendingPinState() throws Exception {
    ScheduledExecutorService extendingThread = async(() -> actuatorOperator.extend(), 5);
    Thread.sleep(15);

    verify(positiveControl, times(1)).setState(PinState.HIGH);
    verify(negativeControl, times(1)).setState(PinState.HIGH);

    extendingThread.shutdownNow();
  }

  @Test
  public void testRetractingPinState() throws Exception {
    ScheduledExecutorService retractingThread = async(() -> actuatorOperator.retract(), 5);
    Thread.sleep(15);

    verify(positiveControl, times(1)).setState(PinState.LOW);
    verify(negativeControl, times(1)).setState(PinState.LOW);

    retractingThread.shutdownNow();
  }

  @Test
  public void testPowerCyclePinState() throws Exception {
    ScheduledExecutorService retractingThread = async(() -> actuatorOperator.retract(), 5);
    Thread.sleep(100);

    actuatorOperator.stop();
    Thread.sleep(STOP_WAIT + 20);

    verify(relayPower, times(1)).setState(PinState.LOW);
    verify(relayPower, times(1)).setState(PinState.HIGH);

    retractingThread.shutdownNow();
  }

  @Test
  public void testCanExtendWhileRetracting() throws Exception {
    ScheduledExecutorService retractingThread = async(() -> actuatorOperator.retract(), 5);
    Thread.sleep(100);

    assertTrue(actuatorOperator.isRetracting());
    assertFalse(actuatorOperator.isExtending());

    ScheduledExecutorService extendingThread = async(() -> actuatorOperator.extend(), 5);
    Thread.sleep(100);

    assertTrue(actuatorOperator.isRetracting());
    assertFalse(actuatorOperator.isExtending());

    retractingThread.shutdownNow();
    extendingThread.shutdownNow();
  }

  private ScheduledExecutorService async(Runnable run, long delay) {
    ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    executor.schedule(() -> run.run(), delay, TimeUnit.MILLISECONDS);
    return executor;
  }
}
