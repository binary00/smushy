package smushy.operate.indicator;


import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class AlertIndicatorTests {

  GpioPinDigitalOutput canSensorIndicator;
  AlertIndicator alertIndicator;

  @Before
  public void setUp() throws Exception {
    canSensorIndicator = mock(GpioPinDigitalOutput.class);
    alertIndicator = new AlertIndicator(canSensorIndicator);
  }

  @Test
  public void testIndicatorInitialTiming() throws Exception {
    Thread.sleep(1100);
    int elapsedSuppressTime = alertIndicator.getElapsedSuppressTime();
    assertEquals(1, elapsedSuppressTime);
  }

  @Test
  public void testIndicatorTiming() throws Exception {
    alertIndicator.indicate();
    Thread.sleep(1100);
    int elapsedIndicateTime = alertIndicator.getElapsedIndicateTime();
    assertEquals(1, elapsedIndicateTime);
  }

  @Test
  public void testIndicatorSuppressTiming() throws Exception {
    alertIndicator.indicate();
    Thread.sleep(500);
    alertIndicator.suppress();
    Thread.sleep(1100);
    int elapsedSuppressTime = alertIndicator.getElapsedSuppressTime();
    assertEquals(1, elapsedSuppressTime);
  }

  @Test
  public void testIndicatorBooleans() throws Exception {
    assertFalse(alertIndicator.isIndicating());
    alertIndicator.indicate();
    assertTrue(alertIndicator.isIndicating());
    alertIndicator.suppress();
    assertFalse(alertIndicator.isIndicating());
  }

  @Test
  public void testIndicatorPinState() throws Exception {
    alertIndicator.indicate();
    verify(canSensorIndicator, times(1)).setState(PinState.HIGH);
  }

  @Test
  public void testIndicatorSuppressPinState() throws Exception {
    alertIndicator.indicate();
    alertIndicator.suppress();
    verify(canSensorIndicator, times(1)).setState(PinState.LOW);
  }

  @Test
  public void testCantIndicateIfAlreadyDoingSo() throws Exception {
    alertIndicator.indicate();
    alertIndicator.indicate();
    verify(canSensorIndicator, times(1)).setState(PinState.HIGH);
  }

  @Test
  public void testCantSuppressIfAlreadyDoingSo() throws Exception {
    alertIndicator.indicate();
    alertIndicator.suppress();
    alertIndicator.suppress();
    verify(canSensorIndicator, times(1)).setState(PinState.LOW);
  }
}
