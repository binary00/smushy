var stomp = null;

function stompConnect() {
  var socket = new SockJS('/websocket');
  stomp = Stomp.over(socket);
  stomp.debug = {}; // Turned off debugging
  stomp.connect({}, stompSubscribe, stompFailureCallback);
}

function stompDisconnect() {
  if(stomp) stomp.disconnect();
}

function stompFailureCallback(error) {
  console.log('STOMP: ' + error);
  setTimeout(stompConnect, 5000);
  console.log('STOMP: Reconecting in 5 seconds');
}

function stompSubscribe() {
  stomp.subscribe('/topic/sensor-state-changes', function(smushyStateMsg) {
    var overallState = JSON.parse(smushyStateMsg.body);
    var smushyState = overallState.smushyState;

    if(overallState.manualDirectState.directing) {
      $('#manual-direct-alert').removeClass('hidden');
      $('#auto-direct-alert').addClass('hidden');
      $('#stop-manual').removeClass('hidden');
    } else {
      $('#manual-direct-alert').addClass('hidden');
      $('#auto-direct-alert').removeClass('hidden');
      $('#stop-manual').addClass('hidden');
    }

    $('#sensor-date').html(moment().utc(smushyState.refreshDate).format("MM/D/Y hh:mm:ss:SSS a"));
    $('#sensor-data').html('' +
        '<li class="list-group-item">Actuator Distance: ' + smushyState.actuatorDistance + '</li>' +
        '<li class="list-group-item">Distance Movement type: ' + smushyState.distanceMovementType + '</li>' +
        '<li class="list-group-item">Actuator Current Usage: ' + smushyState.actuatorCurrentUsage + '</li>' +
        '<li class="list-group-item">Actuating: ' + smushyState.actuating + '</li>' +
        '<li class="list-group-item">Actuator Idle Time: ' + smushyState.actuatorIdleTime + '</li>' +
        '<li class="list-group-item">Actuator Extending: ' + smushyState.actuatorExtending + '</li>' +
        '<li class="list-group-item">Actuator Extending Time: ' + smushyState.actuatorExtendingTime + '</li>' +
        '<li class="list-group-item">Actuator Retracting: ' + smushyState.actuatorRetracting + '</li>' +
        '<li class="list-group-item">Actuator Retracting Time: ' + smushyState.actuatorRetractingTime + '</li>' +
        '<li class="list-group-item">Actuator Last Retract Time: ' + smushyState.actuatorLastRetractTime + '</li>' +
        '<li class="list-group-item">Beam Activated: ' + smushyState.beamActivated + '</li>' +
        '<li class="list-group-item">Beam Activated Time: ' + smushyState.beamActivatedTime + '</li>' +
        '<li class="list-group-item">Beam UnActivated Time: ' + smushyState.beamUnActivatedTime + '</li>' +
        '<li class="list-group-item">Alert Indicating: ' + smushyState.alertIndicating + '</li>' +
        '<li class="list-group-item">Alert Indicate Time: ' + smushyState.alertIndicateTime + '</li>' +
        '<li class="list-group-item">Alert Suppress Time: ' + smushyState.alertSuppressTime + '</li>' +
        '<li class="list-group-item">Door Closed: ' + smushyState.doorClosed + '</li>' +
        '<li class="list-group-item">Door Closed Time: ' + smushyState.doorClosedTime + '</li>' +
        '<li class="list-group-item">Door Open Time: ' + smushyState.doorOpenTime + '</li>' +
        '<li class="list-group-item">Door Last Open Time: ' + smushyState.doorLastOpenTime + '</li>' +
        '<li class="list-group-item">Manually Directing: ' + overallState.manualDirectState.directing + '</li>'
    );
  });
}

function extend() {
  stomp.send("/ws/extend", {}, "");
}

function retract() {
  stomp.send("/ws/retract", {}, "");
}

function stop() {
  stomp.send("/ws/stop", {}, "");
}

function stopManual() {
  stomp.send("/ws/stop-manual", {}, "");
}

$(function () {
  stompConnect();

  $('#extend').click(function() {
    extend();
  });

  $('#retract').click(function() {
    retract();
  });

  $('#stop').click(function() {
    stop();
  });

  $('#stop-manual').click(function() {
    stopManual();
  });
});
