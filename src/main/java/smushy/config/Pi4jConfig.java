package smushy.config;

import com.pi4j.gpio.extension.ads.ADS1115GpioProvider;
import com.pi4j.gpio.extension.ads.ADS1115Pin;
import com.pi4j.io.gpio.*;
import com.pi4j.io.i2c.I2CBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import smushy.operate.actuator.ActuatorOperator;
import smushy.operate.indicator.AlertIndicator;
import smushy.sensor.beam.BeamSensor;
import smushy.sensor.current.CurrentSensor;
import smushy.sensor.distance.DistanceSensor;
import smushy.sensor.distance.tof.TofDistanceSensor;
import smushy.sensor.distance.tof.VL53L0XGpioProvider;
import smushy.sensor.door.DoorSensor;

import static com.pi4j.gpio.extension.ads.ADS1115GpioProvider.ADS1115_ADDRESS_0x48;
import static com.pi4j.gpio.extension.ads.ADS1x15GpioProvider.ProgrammableGainAmplifierValue.PGA_4_096V;
import static com.pi4j.io.gpio.RaspiBcmPin.*;

@Configuration
public class Pi4jConfig {

  static final Logger logger = LoggerFactory.getLogger(Pi4jConfig.class);

  @Bean(destroyMethod = "shutdown")
  public GpioController gpioController() {
    GpioController gpio = null;

    try {
      gpio = GpioFactory.getInstance();
    } catch (Throwable e) {
      logger.error("\n\n------ Unable To Initialize Pi4J! ------\n\n", e);
    }

    return gpio;
  }

  @Bean
  public ADS1115GpioProvider ads1115GpioProvider() {
    ADS1115GpioProvider gpioProvider = null;

    try {
      gpioProvider = new ADS1115GpioProvider(I2CBus.BUS_1, ADS1115_ADDRESS_0x48);
      gpioProvider.setMonitorInterval(100);
    } catch (Throwable e) {
      logger.error("\n\n------ Unable To Initialize Pi4J Analog Provider! ------\n\n", e);
    }

    return gpioProvider;
  }

  @Bean
  public DistanceSensor distanceSensor() {
    TofDistanceSensor tofDistanceSensor = null;

    try {
      VL53L0XGpioProvider vl53L0XGpioProvider = new VL53L0XGpioProvider(I2CBus.BUS_1);
      tofDistanceSensor = new TofDistanceSensor(vl53L0XGpioProvider);
    } catch (Throwable e) {
      logger.error("\n\n------ Unable To Initialize Pi4J VL53L0X Provider ! ------\n\n", e);
    }

    return tofDistanceSensor;
  }

  @Bean
  public CurrentSensor currentSensor(GpioController gpio, ADS1115GpioProvider ads1115GpioProvider, ActuatorOperator actuatorOperator) {
    CurrentSensor currentSensor = null;

    if(gpio != null && ads1115GpioProvider != null) {
      GpioPinAnalogInput extendingCurrentSensor = gpio.provisionAnalogInputPin(ads1115GpioProvider, ADS1115Pin.INPUT_A0, "ExtendingCurrentSensor");
      ads1115GpioProvider.setProgrammableGainAmplifier(PGA_4_096V, ADS1115Pin.INPUT_A0);
      ads1115GpioProvider.setEventThreshold(100, ADS1115Pin.INPUT_A0);

      GpioPinAnalogInput retractingCurrentSensor = gpio.provisionAnalogInputPin(ads1115GpioProvider, ADS1115Pin.INPUT_A1, "RetractingCurrentSensor");
      ads1115GpioProvider.setProgrammableGainAmplifier(PGA_4_096V, ADS1115Pin.INPUT_A1);
      ads1115GpioProvider.setEventThreshold(100, ADS1115Pin.INPUT_A1);

      currentSensor = new CurrentSensor(extendingCurrentSensor, retractingCurrentSensor, ads1115GpioProvider, actuatorOperator);
    }

    return currentSensor;
  }

  @Bean
  public BeamSensor beamSensor(GpioController gpio) {
    BeamSensor beamSensor = null;

    if(gpio != null) {
      GpioPinDigitalInput canBeamSensor = gpio.provisionDigitalInputPin(GPIO_21, "CanBeamSensor", PinPullResistance.PULL_UP);
      beamSensor = new BeamSensor(canBeamSensor);
    }

    return beamSensor;
  }

  @Bean
  public ActuatorOperator actuatorOperator(GpioController gpio) {
    ActuatorOperator actuatorDirector = null;

    if(gpio != null) {
      GpioPinDigitalOutput positiveControl = gpio.provisionDigitalOutputPin(GPIO_02, "PositiveControl", PinState.LOW);
      GpioPinDigitalOutput negativeControl = gpio.provisionDigitalOutputPin(GPIO_07, "NegativeControl", PinState.LOW);
      GpioPinDigitalOutput relayPower = gpio.provisionDigitalOutputPin(GPIO_06, "RelayPower", PinState.LOW);

      return new ActuatorOperator(positiveControl, negativeControl, relayPower);
    }

    return actuatorDirector;
  }

  @Bean
  public AlertIndicator alertIndicator(GpioController gpio) {
    AlertIndicator alertIndicator = null;

    if(gpio != null) {
      GpioPinDigitalOutput canSensorIndicator =
          gpio.provisionDigitalOutputPin(GPIO_29, "CanSensorIndicator", PinState.LOW);

      alertIndicator = new AlertIndicator(canSensorIndicator);
    }

    return alertIndicator;
  }

  @Bean
  public DoorSensor doorSensor(GpioController gpio) {
    DoorSensor doorSensor = null;

    if(gpio != null) {
      GpioPinDigitalInput doorSensorInput = gpio.provisionDigitalInputPin(GPIO_28, "DoorSensor", PinPullResistance.PULL_UP);
      doorSensor = new DoorSensor(doorSensorInput);
    }

    return doorSensor;
  }
}
