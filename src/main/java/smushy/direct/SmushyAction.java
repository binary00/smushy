package smushy.direct;

public enum SmushyAction {

  INDICATE,
  RETRACT,
  CRUSH,
  STOP,
  RESET,
  NOTHING

}
