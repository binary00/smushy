package smushy.direct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import smushy.direct.state.ManualDirectState;

import static smushy.direct.SmushyAction.*;

@Controller
public class SmushyController {

  static final Logger logger = LoggerFactory.getLogger(SmushyController.class);

  @Autowired private ManualDirectState manualDirectState;

  @MessageMapping("/extend")
  public void extend() {
    manualDirectState.setSmushyAction(CRUSH);
  }

  @MessageMapping("/retract")
  public void retract() {
    manualDirectState.setSmushyAction(RETRACT);
  }

  @MessageMapping("/stop")
  public void stop() {
    manualDirectState.setSmushyAction(STOP);
  }

  @MessageMapping("/stop-manual")
  public void stopManualDirecting() {
    manualDirectState.setDirecting(false);
  }
}
