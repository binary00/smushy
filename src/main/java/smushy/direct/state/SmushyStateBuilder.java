package smushy.direct.state;

import smushy.operate.actuator.ActuatorOperator;
import smushy.operate.indicator.AlertIndicator;
import smushy.sensor.beam.BeamSensor;
import smushy.sensor.current.CurrentSensor;
import smushy.sensor.distance.DistanceMovementType;
import smushy.sensor.distance.DistanceSensor;
import smushy.sensor.door.DoorSensor;

public class SmushyStateBuilder {

  private Double actuatorDistance;
  private DistanceMovementType distanceMovementType;
  private Double actuatorCurrentUsage;
  private boolean actuating;
  private boolean actuatorExtending;
  private boolean actuatorRetracting;
  private int actuatorExtendingTime;
  private int actuatorRetractingTime;
  private int actuatorIdleTime;
  private int actuatorLastRetractTime;
  private boolean beamActivated;
  private int beamActivatedTime;
  private int beamUnActivatedTime;
  private boolean alertIndicating;
  private int alertIndicateTime;
  private int alertSuppressTime;
  private boolean doorClosed;
  private int doorClosedTime;
  private int doorOpenTime;
  private int doorLastOpenTime;
  private long refreshDate;

  private SmushyStateBuilder() {
    // No instance
  }

  public SmushyStateBuilder setActuatorDistance(Double actuatorDistance) {
    this.actuatorDistance = actuatorDistance;
    return this;
  }

  public SmushyStateBuilder setDistanceMovementType(DistanceMovementType distanceMovementType) {
    this.distanceMovementType = distanceMovementType;
    return this;
  }

  public SmushyStateBuilder setActuatorCurrentUsage(Double actuatorCurrentUsage) {
    this.actuatorCurrentUsage = actuatorCurrentUsage;
    return this;
  }

  public SmushyStateBuilder setActuating(boolean actuating) {
    this.actuating = actuating;
    return this;
  }

  public SmushyStateBuilder setActuatorExtending(boolean actuatorExtending) {
    this.actuatorExtending = actuatorExtending;
    return this;
  }

  public SmushyStateBuilder setActuatorRetracting(boolean actuatorRetracting) {
    this.actuatorRetracting = actuatorRetracting;
    return this;
  }

  public SmushyStateBuilder setActuatorExtendingTime(int actuatorExtendingTime) {
    this.actuatorExtendingTime = actuatorExtendingTime;
    return this;
  }

  public SmushyStateBuilder setActuatorRetractingTime(int actuatorRetractingTime) {
    this.actuatorRetractingTime = actuatorRetractingTime;
    return this;
  }

  public SmushyStateBuilder setActuatorIdleTime(int actuatorIdleTime) {
    this.actuatorIdleTime = actuatorIdleTime;
    return this;
  }

  public SmushyStateBuilder setActuatorLastRetractTime(int actuatorLastRetractTime) {
    this.actuatorLastRetractTime = actuatorLastRetractTime;
    return this;
  }

  public SmushyStateBuilder setBeamActivated(boolean beamActivated) {
    this.beamActivated = beamActivated;
    return this;
  }

  public SmushyStateBuilder setBeamActivatedTime(int beamActivatedTime) {
    this.beamActivatedTime = beamActivatedTime;
    return this;
  }

  public SmushyStateBuilder setBeamUnActivatedTime(int beamUnActivatedTime) {
    this.beamUnActivatedTime = beamUnActivatedTime;
    return this;
  }

  public SmushyStateBuilder setAlertIndicating(boolean alertIndicating) {
    this.alertIndicating = alertIndicating;
    return this;
  }

  public SmushyStateBuilder setAlertIndicateTime(int alertIndicateTime) {
    this.alertIndicateTime = alertIndicateTime;
    return this;
  }

  public SmushyStateBuilder setAlertSuppressTime(int alertSuppressTime) {
    this.alertSuppressTime = alertSuppressTime;
    return this;
  }

  public SmushyStateBuilder setDoorClosed(boolean doorClosed) {
    this.doorClosed = doorClosed;
    return this;
  }

  public SmushyStateBuilder setDoorClosedTime(int doorClosedTime) {
    this.doorClosedTime = doorClosedTime;
    return this;
  }

  public SmushyStateBuilder setDoorOpenTime(int doorOpenTime) {
    this.doorOpenTime = doorOpenTime;
    return this;
  }

  public SmushyStateBuilder setDoorLastOpenTime(int doorLastOpenTime) {
    this.doorLastOpenTime = doorLastOpenTime;
    return this;
  }

  public SmushyStateBuilder setRefreshDate(long refreshDate) {
    this.refreshDate = refreshDate;
    return this;
  }

  private SmushyState createSmushyState() {
    return new SmushyState(actuatorDistance, distanceMovementType, actuatorCurrentUsage, actuating, actuatorExtending,
        actuatorRetracting, actuatorExtendingTime, actuatorRetractingTime, actuatorIdleTime, actuatorLastRetractTime,
        beamActivated, beamActivatedTime, beamUnActivatedTime, alertIndicating, alertIndicateTime, alertSuppressTime,
        doorClosed, doorClosedTime, doorOpenTime, doorLastOpenTime, refreshDate);
  }

  public static SmushyState createSmushyState(DistanceSensor distanceSensor,
                                                 CurrentSensor currentSensor,
                                                 BeamSensor beamSensor,
                                                 DoorSensor doorSensor,
                                                 AlertIndicator alertIndicator,
                                                 ActuatorOperator actuatorOperator) {

    SmushyStateBuilder builder = new SmushyStateBuilder();

    if(distanceSensor != null) {
      builder.setActuatorDistance(distanceSensor.getDistance());
      builder.setDistanceMovementType(distanceSensor.getMovementType());
    }

    if(currentSensor != null) {
      builder.setActuatorCurrentUsage(currentSensor.getCurrent());
    }

    if(beamSensor != null) {
      builder.setBeamActivated(beamSensor.isActivated())
          .setBeamActivatedTime(beamSensor.getElapsedActivatedTime())
          .setBeamUnActivatedTime(beamSensor.getElapsedUnActivatedTime());
    }

    if(doorSensor != null) {
      builder.setDoorClosed(!doorSensor.isActivated())
          .setDoorClosedTime(doorSensor.getElapsedUnActivatedTime())
          .setDoorOpenTime(doorSensor.getElapsedActivatedTime())
          .setDoorLastOpenTime(doorSensor.getDoorLastOpenTime());
    }

    if(alertIndicator != null) {
      builder.setAlertIndicating(alertIndicator.isIndicating())
          .setAlertIndicateTime(alertIndicator.getElapsedIndicateTime())
          .setAlertSuppressTime(alertIndicator.getElapsedSuppressTime());
    }

    if(actuatorOperator != null) {
      builder.setActuating(actuatorOperator.isActuating())
          .setActuatorExtending(actuatorOperator.isExtending())
          .setActuatorRetracting(actuatorOperator.isRetracting())
          .setActuatorExtendingTime(actuatorOperator.getElapsedExtendingTime())
          .setActuatorRetractingTime(actuatorOperator.getElapsedRetractingTime())
          .setActuatorIdleTime(actuatorOperator.getElapsedIdleTime())
          .setActuatorLastRetractTime(actuatorOperator.getLastRetractedTime());
    }

    builder.setRefreshDate(System.currentTimeMillis());

    return builder.createSmushyState();
  }
}