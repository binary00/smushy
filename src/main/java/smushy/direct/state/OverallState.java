package smushy.direct.state;

public class OverallState {

  private SmushyState smushyState;
  private ManualDirectState manualDirectState;

  public OverallState(SmushyState smushyState, ManualDirectState manualDirectState) {
    this.smushyState = smushyState;
    this.manualDirectState = manualDirectState;
  }

  public SmushyState getSmushyState() {
    return smushyState;
  }

  public ManualDirectState getManualDirectState() {
    return manualDirectState;
  }
}
