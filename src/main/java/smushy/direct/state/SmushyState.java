package smushy.direct.state;


import smushy.sensor.distance.DistanceMovementType;

public class SmushyState {

  private Double actuatorDistance;
  private DistanceMovementType distanceMovementType;
  private Double actuatorCurrentUsage;
  private boolean actuating;
  private boolean actuatorExtending;
  private boolean actuatorRetracting;
  private int actuatorExtendingTime;
  private int actuatorRetractingTime;
  private int actuatorIdleTime;
  private int actuatorLastRetractTime;

  private boolean beamActivated;
  private int beamActivatedTime;
  private int beamUnActivatedTime;

  private boolean alertIndicating;
  private int alertIndicateTime;
  private int alertSuppressTime;

  private boolean doorClosed;
  private int doorClosedTime;
  private int doorOpenTime;
  private int doorLastOpenTime;

  private long refreshDate;

  public SmushyState(Double actuatorDistance, DistanceMovementType distanceMovementType, Double actuatorCurrentUsage,
                     boolean actuating, boolean actuatorExtending, boolean actuatorRetracting,
                     int actuatorExtendingTime, int actuatorRetractingTime, int actuatorIdleTime,
                     int actuatorLastRetractTime, boolean beamActivated, int beamActivatedTime,
                     int beamUnActivatedTime, boolean alertIndicating, int alertIndicateTime, int alertSuppressTime,
                     boolean doorClosed, int doorClosedTime, int doorOpenTime, int doorLastOpenTime, long refreshDate) {

    this.actuatorDistance = actuatorDistance;
    this.distanceMovementType = distanceMovementType;
    this.actuatorCurrentUsage = actuatorCurrentUsage;
    this.actuating = actuating;
    this.actuatorExtending = actuatorExtending;
    this.actuatorRetracting = actuatorRetracting;
    this.actuatorExtendingTime = actuatorExtendingTime;
    this.actuatorRetractingTime = actuatorRetractingTime;
    this.actuatorIdleTime = actuatorIdleTime;
    this.actuatorLastRetractTime = actuatorLastRetractTime;
    this.beamActivated = beamActivated;
    this.beamActivatedTime = beamActivatedTime;
    this.beamUnActivatedTime = beamUnActivatedTime;
    this.alertIndicating = alertIndicating;
    this.alertIndicateTime = alertIndicateTime;
    this.alertSuppressTime = alertSuppressTime;
    this.doorClosed = doorClosed;
    this.doorClosedTime = doorClosedTime;
    this.doorOpenTime = doorOpenTime;
    this.doorLastOpenTime = doorLastOpenTime;
    this.refreshDate = refreshDate;
  }

  public Double getActuatorDistance() {
    return actuatorDistance;
  }

  public DistanceMovementType getDistanceMovementType() {
    return distanceMovementType;
  }

  public Double getActuatorCurrentUsage() {
    return actuatorCurrentUsage;
  }

  public boolean isActuating() {
    return actuating;
  }

  public boolean isActuatorExtending() {
    return actuatorExtending;
  }

  public boolean isActuatorRetracting() {
    return actuatorRetracting;
  }

  public int getActuatorExtendingTime() {
    return actuatorExtendingTime;
  }

  public int getActuatorRetractingTime() {
    return actuatorRetractingTime;
  }

  public int getActuatorIdleTime() {
    return actuatorIdleTime;
  }

  public int getActuatorLastRetractTime() {
    return actuatorLastRetractTime;
  }

  public boolean isBeamActivated() {
    return beamActivated;
  }

  public int getBeamActivatedTime() {
    return beamActivatedTime;
  }

  public int getBeamUnActivatedTime() {
    return beamUnActivatedTime;
  }

  public boolean isAlertIndicating() {
    return alertIndicating;
  }

  public int getAlertIndicateTime() {
    return alertIndicateTime;
  }

  public int getAlertSuppressTime() {
    return alertSuppressTime;
  }

  public boolean isDoorClosed() {
    return doorClosed;
  }

  public int getDoorClosedTime() {
    return doorClosedTime;
  }

  public int getDoorOpenTime() {
    return doorOpenTime;
  }

  public int getDoorLastOpenTime() {
    return doorLastOpenTime;
  }

  public long getRefreshDate() {
    return refreshDate;
  }
}
