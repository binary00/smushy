package smushy.direct.state;

import org.springframework.stereotype.Component;
import smushy.direct.SmushyAction;

import static smushy.direct.SmushyAction.NOTHING;

@Component
public class ManualDirectState {

  private boolean isDirecting = false;
  private SmushyAction smushyAction = NOTHING;

  public boolean isDirecting() {
    return isDirecting;
  }

  public void setDirecting(boolean directing) {
    this.isDirecting = directing;
    if(!isDirecting) {
      this.smushyAction = NOTHING;
    }
  }

  public SmushyAction getSmushyAction() {
    return smushyAction;
  }

  public void setSmushyAction(SmushyAction smushyAction) {
    this.isDirecting = true;
    this.smushyAction = smushyAction;
  }
}
