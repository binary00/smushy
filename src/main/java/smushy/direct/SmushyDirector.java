package smushy.direct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import smushy.direct.evaluate.SmushyEvaluator;
import smushy.direct.state.OverallState;
import smushy.operate.actuator.ActuatorOperator;
import smushy.operate.indicator.AlertIndicator;

@Component
public class SmushyDirector {

  static final Logger logger = LoggerFactory.getLogger(SmushyDirector.class);

  @Autowired private AlertIndicator alertIndicator;
  @Autowired private ActuatorOperator actuatorOperator;

  @Autowired private SimpMessagingTemplate messagingTemplate;
  @Autowired private SmushyEvaluator smushyEvaluator;

  @Scheduled(fixedDelay = 250)
  protected void refresh() {
    direct(smushyEvaluator.getAction());
    publishState(smushyEvaluator.overallState());
  }

  protected void direct(SmushyAction smushyAction) {
    switch(smushyAction) {
      case INDICATE:
        startIndicating();
        break;
      case CRUSH:
        startCrushing();
        break;
      case RETRACT:
        retractCrusher();
        break;
      case STOP:
        fullStop();
        break;
      case RESET:
        retractCrusher();
        break;
      case NOTHING:
        break;
    }
  }

  private void startIndicating() {
    alertIndicator.indicate();
  }

  private void startCrushing() {
    alertIndicator.indicate();
    actuatorOperator.extend();
  }

  private void retractCrusher() {
    alertIndicator.suppress();
    actuatorOperator.retract();
  }

  private void fullStop() {
    alertIndicator.suppress();
    actuatorOperator.stop();
  }

  private void publishState(OverallState overallState) {
    messagingTemplate.convertAndSend("/topic/sensor-state-changes", overallState);
  }
}
