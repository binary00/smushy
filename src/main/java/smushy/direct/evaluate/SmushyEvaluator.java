package smushy.direct.evaluate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import smushy.direct.SmushyAction;
import smushy.direct.SmushyDirector;
import smushy.direct.state.ManualDirectState;
import smushy.direct.state.OverallState;
import smushy.direct.state.SmushyState;
import smushy.direct.state.SmushyStateBuilder;
import smushy.operate.actuator.ActuatorOperator;
import smushy.operate.indicator.AlertIndicator;
import smushy.sensor.beam.BeamSensor;
import smushy.sensor.current.CurrentSensor;
import smushy.sensor.distance.DistanceSensor;
import smushy.sensor.door.DoorSensor;

import static smushy.direct.SmushyAction.*;
import static smushy.direct.evaluate.SmushyEvaluatorUtils.*;

@Component
public class SmushyEvaluator {

  static final Logger logger = LoggerFactory.getLogger(SmushyDirector.class);

  @Autowired private DistanceSensor distanceSensor;
  @Autowired private CurrentSensor currentSensor;
  @Autowired private BeamSensor beamSensor;
  @Autowired private DoorSensor doorSensor;

  @Autowired private AlertIndicator alertIndicator;
  @Autowired private ActuatorOperator actuatorOperator;

  @Autowired private ManualDirectState manualDirectState;

  private SmushyState smushyState;
  private SmushyAction smushyAction = NOTHING;

  @Scheduled(fixedDelay = 200)
  protected void refresh() {
    if(areComponentsInitialized(distanceSensor,
                                currentSensor,
                                beamSensor,
                                doorSensor,
                                alertIndicator,
                                actuatorOperator)) {

      smushyState = SmushyStateBuilder.createSmushyState(distanceSensor,
                                                         currentSensor,
                                                         beamSensor,
                                                         doorSensor,
                                                         alertIndicator,
                                                         actuatorOperator);

      if(manualDirectState.isDirecting()) {
        smushyAction = manualDirectState.getSmushyAction();
      } else {

        smushyAction = autoDirect();
      }
    }
  }

  public SmushyAction getAction() {
    return smushyAction;
  }

  public OverallState overallState() {
    return new OverallState(smushyState, manualDirectState);
  }

  private SmushyAction autoDirect() {
    if(shouldShowInitialIndicator(smushyState)) {
      logger.info("Indicate");
      return INDICATE;
    } else if(shouldSuppressInitialIndicator(smushyState)) {
      logger.info("Suppress");
      return RETRACT;
    } else if(shouldCrush(smushyState)) {
      logger.info("Crush");
      return CRUSH;
    } else if(shouldStopCrushing(smushyState)) {
      logger.info("Stop");
      return RETRACT;
    } else if(shouldReset(smushyState)) {
      logger.info("Reset");
      return RESET;
    }

    return NOTHING;
  }
}
