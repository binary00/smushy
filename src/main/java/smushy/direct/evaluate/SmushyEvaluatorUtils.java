package smushy.direct.evaluate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smushy.direct.state.SmushyState;
import smushy.operate.actuator.ActuatorOperator;
import smushy.operate.indicator.AlertIndicator;
import smushy.sensor.beam.BeamSensor;
import smushy.sensor.current.CurrentSensor;
import smushy.sensor.distance.DistanceMovementType;
import smushy.sensor.distance.DistanceSensor;
import smushy.sensor.door.DoorSensor;

public class SmushyEvaluatorUtils {

  static final Logger logger = LoggerFactory.getLogger(SmushyEvaluatorUtils.class);

  // Distances are how far away the crushing edge of the actuator
  // is from the top of whatever the can is sitting on.
  protected static final Double RETRACTED_DISTANCE = 8.40D; // Inches
  protected static final Double MAX_EXTENDED_DISTANCE = .90D; // Inches

  protected static final Double MAX_CURRENT_THRESHOLD = 14.5D; // Amps
  protected static final Double NO_LOAD_CURRENT = 0D; // Amps

  protected static final int BEAM_PRE_ACTUATE_REST_TIME = 3; // Seconds
  protected static final int GLOBAL_REST_TIME = 3; // Seconds
  protected static final int CURRENT_LOAD_BUFFER_TIME = 2;

  private SmushyEvaluatorUtils() {
    // Can't create instance
  }

  protected static boolean shouldShowInitialIndicator(SmushyState smushyState) {
    return smushyState.isBeamActivated()
        && smushyState.isDoorClosed()
        && metAlertSuppressThreshold(smushyState)
        && metActuatorIdleThreshold(smushyState)
        && isActuatorFullyRetracted(smushyState)
        && metPostCrushDoorOpenCheck(smushyState);
  }

  protected static boolean shouldCrush(SmushyState smushyState) {
    return metBeamActivatedPreActuateThreshold(smushyState)
        && metActuatorIdleThreshold(smushyState)
        && isActuatorFullyRetracted(smushyState)
        && metPostCrushDoorOpenCheck(smushyState)
        && smushyState.isDoorClosed();
  }

  protected static boolean shouldStopCrushing(SmushyState smushyState) {
    if(smushyState.isActuatorExtending()) {

     /* System.out.println("At Max Extension: " + isActuatorAtMaxExtension(smushyState));
      System.out.println("Exceeds Current Limit: " + exceedingCurrentLimit(smushyState));
      System.out.println("Beam Not Activated: " + !smushyState.isBeamActivated());
      System.out.println("Door Not Closed: " + !smushyState.isDoorClosed());
      System.out.println("Fails Crushing Verification: " + failsCrushingVerification(smushyState));
      System.out.println("\n\n");*/

      return isActuatorAtMaxExtension(smushyState)
          || exceedingCurrentLimit(smushyState)
          || !smushyState.isBeamActivated()
          || !smushyState.isDoorClosed()
          || failsCrushingVerification(smushyState);
    }

    return false;
  }

  protected static boolean shouldSuppressInitialIndicator(SmushyState smushyState) {
    return metAlertInitialIndicatorThreshold(smushyState)
        && metBeamUnActivatedThreshold(smushyState)
        && metActuatorIdleThreshold(smushyState)
        && isActuatorFullyRetracted(smushyState);
  }

  protected static boolean shouldReset(SmushyState smushyState) {
    return smushyState.getActuatorIdleTime() >= GLOBAL_REST_TIME
           && !isActuatorFullyRetracted(smushyState);
  }

  protected static boolean failsCrushingVerification(SmushyState smushyState) {
    DistanceMovementType movementType = smushyState.getDistanceMovementType();

    /*System.out.println("\n Crushing Verification");
    System.out.println("Invalid Movement Type: " + movementType.equals(DistanceMovementType.RECEDING));
    System.out.println("Not Using Current: " + hasNoCurrentLoad(smushyState) + "\n\n");*/

    return movementType.equals(DistanceMovementType.RECEDING)
        || hasNoCurrentLoad(smushyState);
  }

  protected static boolean areComponentsInitialized(DistanceSensor distanceSensor, CurrentSensor currentSensor,
                                                    BeamSensor beamSensor, DoorSensor doorSensor,
                                                    AlertIndicator alertIndicator, ActuatorOperator actuatorOperator) {
    return distanceSensor != null && currentSensor != null
        && beamSensor != null && doorSensor != null
        && alertIndicator != null && actuatorOperator != null;
  }

  protected static boolean isActuatorFullyRetracted(SmushyState smushyState) {

    if(!smushyState.isActuating() && smushyState.getActuatorDistance() != null) {
      return RETRACTED_DISTANCE.compareTo(smushyState.getActuatorDistance()) < 0
          && hasNoCurrentLoad(smushyState);
    }

    return false;
  }

  protected static boolean isActuatorAtMaxExtension(SmushyState smushyState) {

    if(smushyState.isActuating() && smushyState.getActuatorDistance() != null) {
      return MAX_EXTENDED_DISTANCE.compareTo(smushyState.getActuatorDistance()) > 0;
    }

    return false;
  }

  protected static boolean exceedingCurrentLimit(SmushyState smushyState) {
    if(smushyState.getActuatorCurrentUsage() != null) {
      /*System.out.println("Current usage: " + smushyState.getActuatorCurrentUsage());*/
      if(smushyState.isActuatorExtending()) {
        int actuatorExtendingTime = smushyState.getActuatorExtendingTime();

        return MAX_CURRENT_THRESHOLD.compareTo(smushyState.getActuatorCurrentUsage()) < 0
               && actuatorExtendingTime >= CURRENT_LOAD_BUFFER_TIME;
      } else if(smushyState.isActuatorRetracting()) {
        int actuatorRetractingTime = smushyState.getActuatorRetractingTime();

        return MAX_CURRENT_THRESHOLD.compareTo(smushyState.getActuatorCurrentUsage()) < 0
               && actuatorRetractingTime >= CURRENT_LOAD_BUFFER_TIME;
      } else {
        return MAX_CURRENT_THRESHOLD.compareTo(smushyState.getActuatorCurrentUsage()) < 0;
      }
    }

    return false;
  }

  protected static boolean hasNoCurrentLoad(SmushyState smushyState) {
    if(smushyState.getActuatorCurrentUsage() != null) {
      if(smushyState.isActuatorExtending()) {
        int actuatorExtendingTime = smushyState.getActuatorExtendingTime();

        return NO_LOAD_CURRENT.compareTo(smushyState.getActuatorCurrentUsage()) >= 0
            && actuatorExtendingTime >= CURRENT_LOAD_BUFFER_TIME;
      } else if(smushyState.isActuatorRetracting()) {
        int actuatorRetractingTime = smushyState.getActuatorRetractingTime();

        return NO_LOAD_CURRENT.compareTo(smushyState.getActuatorCurrentUsage()) >= 0
            && actuatorRetractingTime >= CURRENT_LOAD_BUFFER_TIME;
      } else {
        return NO_LOAD_CURRENT.compareTo(smushyState.getActuatorCurrentUsage()) >= 0;
      }
    }

    return true;
  }

  protected static boolean metAlertInitialIndicatorThreshold(SmushyState smushyState) {
    return smushyState.isAlertIndicating()
        && GLOBAL_REST_TIME <= smushyState.getAlertIndicateTime();
  }

  protected static boolean metAlertSuppressThreshold(SmushyState smushyState) {
    return !smushyState.isAlertIndicating()
        && GLOBAL_REST_TIME <= smushyState.getAlertSuppressTime();
  }

  protected static boolean metActuatorIdleThreshold(SmushyState smushyState) {
    return !smushyState.isActuating()
        && GLOBAL_REST_TIME <= smushyState.getActuatorIdleTime();
  }

  protected static boolean metBeamUnActivatedThreshold(SmushyState smushyState) {
    return !smushyState.isBeamActivated()
        && GLOBAL_REST_TIME <= smushyState.getBeamUnActivatedTime();
  }

  protected static boolean metBeamActivatedPreActuateThreshold(SmushyState smushyState) {
    return smushyState.isBeamActivated()
        && BEAM_PRE_ACTUATE_REST_TIME <= smushyState.getBeamActivatedTime();
  }

  protected static boolean metPostCrushDoorOpenCheck(SmushyState smushyState) {
    return smushyState.getActuatorIdleTime() >= smushyState.getDoorLastOpenTime()
           && isActuatorFullyRetracted(smushyState);
  }
}
