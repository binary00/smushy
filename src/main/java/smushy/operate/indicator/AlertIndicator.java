package smushy.operate.indicator;


import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;

import static smushy.utils.TimeUtils.calculateElapsedTimeInSeconds;

public class AlertIndicator {

  private GpioPinDigitalOutput canSensorIndicator;
  private boolean isIndicating;

  private long indicateTime = -1;
  private long suppressTime = -1;

  public AlertIndicator(GpioPinDigitalOutput canSensorIndicator) {
    this.canSensorIndicator = canSensorIndicator;
    suppressTime = System.currentTimeMillis(); // Initial state is off.
  }

  public void indicate() {
    if(!isIndicating) {
      isIndicating = true;
      suppressTime = -1;
      indicateTime = System.currentTimeMillis();

      canSensorIndicator.setState(PinState.HIGH);
    }
  }

  public void suppress() {
    if(isIndicating) {
      isIndicating = false;
      indicateTime = -1;
      suppressTime = System.currentTimeMillis();

      canSensorIndicator.setState(PinState.LOW);
    }
  }

  public boolean isIndicating() {
    return isIndicating;
  }

  public int getElapsedIndicateTime() {
    return calculateElapsedTimeInSeconds(indicateTime);
  }

  public int getElapsedSuppressTime() {
    return calculateElapsedTimeInSeconds(suppressTime);
  }
}
