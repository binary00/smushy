package smushy.operate.actuator;

import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;

import javax.annotation.PreDestroy;
import java.util.function.Supplier;

import static smushy.utils.TimeUtils.calculateElapsedTimeInSeconds;

public class ActuatorOperator {

  static final Logger logger = LoggerFactory.getLogger(ActuatorOperator.class);

  private boolean isExtending;
  private boolean isRetracting;

  private GpioPinDigitalOutput positiveControl;
  private GpioPinDigitalOutput negativeControl;
  private GpioPinDigitalOutput relayPower;

  protected static final long POWER_CYCLE_RATE = 100;
  protected static final long STOP_WAIT = POWER_CYCLE_RATE + 10;
  protected static final int RETRACT_BUFFER = 3;
  protected static final int MAX_RETRACT_TIME = 15;

  private long extendTime = -1;
  private long retractTime = -1;
  private long idleTime = -1;
  private long extendStartTime = -1;
  private long lastRetractTime = -1;
  private boolean stop = false;

  public ActuatorOperator(GpioPinDigitalOutput positiveControl, GpioPinDigitalOutput negativeControl,
                          GpioPinDigitalOutput relayPower) {

    this.positiveControl = positiveControl;
    this.negativeControl = negativeControl;
    this.relayPower = relayPower;
    idleTime = System.currentTimeMillis();
  }

  @Async
  public void extend() {
    if(!isActuating()) {
      logger.debug("Extending.");

      stop = false;
      isExtending = true;
      isRetracting = false;
      timer();
      extendStartTime = System.currentTimeMillis();

      positiveControl.setState(PinState.HIGH);
      negativeControl.setState(PinState.HIGH);
      extendCycle();

      isExtending = false;
      timer();
    }
  }

  @Async
  public void retract() {
    if(!isRetracting) {
      logger.debug("Retracting.");
      if(isExtending) stop();

      stop = false;
      isExtending = false;
      isRetracting = true;
      timer();

      positiveControl.setState(PinState.LOW);
      negativeControl.setState(PinState.LOW);
      int runTime = calculateElapsedTimeInSeconds(extendStartTime) + RETRACT_BUFFER;
      retractCycle(() -> getElapsedRetractingTime(), runTime);

      isRetracting = false;
      timer();
      lastRetractTime = System.currentTimeMillis();
    }
  }

  private void extendCycle() {
    relayPower.setState(PinState.HIGH);

    do {
      try {
        Thread.sleep(POWER_CYCLE_RATE);
      } catch (InterruptedException e) {
        relayPower.setState(PinState.LOW);
      }
    } while(!stop);

    relayPower.setState(PinState.LOW);
  }

  private void retractCycle(Supplier<Integer> elapsedTime, int elapsedRunTime) {
    relayPower.setState(PinState.HIGH);

    do {
      try {
        Thread.sleep(POWER_CYCLE_RATE);
      } catch (InterruptedException e) {
        relayPower.setState(PinState.LOW);
      }
    } while(!stop && (elapsedTime.get() < elapsedRunTime) && (elapsedTime.get() < MAX_RETRACT_TIME));

    relayPower.setState(PinState.LOW);
  }

  public void stop() {
    stop = true;
    try {
      Thread.sleep(STOP_WAIT);
    } catch (InterruptedException e) {
    }
  }

  private void timer() {
    if(isExtending) {
      idleTime = -1;
      retractTime = -1;
      extendTime = System.currentTimeMillis();
    } else if(isRetracting) {
      idleTime = -1;
      extendTime = -1;
      retractTime = System.currentTimeMillis();
    } else if(!isActuating()) {
      extendTime = -1;
      retractTime = -1;
      idleTime = System.currentTimeMillis();
    }
  }

  @PreDestroy
  public void shutDown() {
    stop = true;
    relayPower.setState(PinState.LOW);
  }

  public boolean isExtending() {
    return isExtending;
  }

  public boolean isRetracting() {
    return isRetracting;
  }

  public boolean isActuating() {
    return isExtending || isRetracting;
  }

  public int getElapsedExtendingTime() {
    return calculateElapsedTimeInSeconds(extendTime);
  }

  public int getElapsedRetractingTime() {
    return calculateElapsedTimeInSeconds(retractTime);
  }

  public int getElapsedIdleTime() {
    return calculateElapsedTimeInSeconds(idleTime);
  }

  public int getLastRetractedTime() {
    return calculateElapsedTimeInSeconds(lastRetractTime);
  }
}
