package smushy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmushyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmushyApplication.class, args);
	}
}
