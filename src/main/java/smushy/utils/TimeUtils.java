package smushy.utils;


public class TimeUtils {

  public static int calculateElapsedTimeInSeconds(long startTime) {
    if(startTime > 0) {
      long elapsedTime = System.currentTimeMillis() - startTime;

      // Only care about seconds
      if(elapsedTime >= 1000 && elapsedTime < Integer.MAX_VALUE) {
        return Math.toIntExact(elapsedTime / 1000);
      } else if(elapsedTime >= Integer.MAX_VALUE) {
        return Integer.MAX_VALUE;
      }
    }

    return 0;
  }

}
