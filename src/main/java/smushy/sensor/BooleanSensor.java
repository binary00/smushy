package smushy.sensor;


import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static smushy.utils.TimeUtils.calculateElapsedTimeInSeconds;

public abstract class BooleanSensor {

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  private boolean activated;

  private long activatedTime = -1;
  private long unActivatedTime = -1;

  public BooleanSensor(GpioPinDigitalInput sensorInput) {
    logger.info(getClass().getSimpleName() + " initialized.");

    unActivatedTime = System.currentTimeMillis();
    sensorInput.addListener((GpioPinListenerDigital) event -> handleCallback(event));
  }

  protected void handleCallback(GpioPinDigitalStateChangeEvent event) {
    if(event.getState().isHigh()) {
      setUnactivatedData();
    } else {
      setActivatedData();
    }

    logger.debug(getClass().getSimpleName() + " activated: " + activated);
  }

  protected void setActivatedData() {
    activatedTime = System.currentTimeMillis();
    activated = true;
  }

  protected void setUnactivatedData() {
    unActivatedTime = System.currentTimeMillis();
    activated = false;
  }

  protected long getActivatedTime() {
    return activatedTime;
  }

  protected long getUnActivatedTime() {
    return unActivatedTime;
  }

  public boolean isActivated() {
    return activated;
  }

  public int getElapsedActivatedTime() {
    if(activated) {
      return calculateElapsedTimeInSeconds(activatedTime);
    }

    return 0;
  }

  public int getElapsedUnActivatedTime() {
    if(!activated) {
      return calculateElapsedTimeInSeconds(unActivatedTime);
    }

    return 0;
  }
}
