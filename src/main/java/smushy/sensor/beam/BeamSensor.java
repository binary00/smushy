package smushy.sensor.beam;

import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import smushy.sensor.BooleanSensor;

public class BeamSensor extends BooleanSensor {

  public BeamSensor(GpioPinDigitalInput sensorInput) {
    super(sensorInput);
  }

  @Override
  protected void handleCallback(GpioPinDigitalStateChangeEvent event) {
    super.handleCallback(event);
  }
}
