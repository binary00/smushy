package smushy.sensor.distance;

public enum DistanceMovementType {

  APPROACHING,
  RECEDING,
  STOPPED,
  UNDETERMINED

}
