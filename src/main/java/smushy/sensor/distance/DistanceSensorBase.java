package smushy.sensor.distance;

import com.google.common.collect.EvictingQueue;
import com.google.common.collect.Queues;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Queue;

import static smushy.sensor.distance.DistanceUtils.determineMovementType;

public class DistanceSensorBase implements DistanceSensor {

  protected Double distance;
  protected Queue<Double> distanceHistory = Queues.synchronizedQueue(EvictingQueue.create(8));
  protected DistanceMovementType movementType = DistanceMovementType.STOPPED;

  @Scheduled(fixedDelay = 300)
  protected void setMovementType () {
    movementType = determineMovementType(distanceHistory);
  }

  @Override
  public Double getDistance() {
    return distance;
  }

  @Override
  public DistanceMovementType getMovementType() {
    return movementType;
  }
}
