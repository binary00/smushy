package smushy.sensor.distance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Queue;

public class DistanceUtils {

  static final Logger logger = LoggerFactory.getLogger(DistanceUtils.class);

  protected static final Double DISTANCE_SIGNIFICANCE = .20;
  protected static final Double INVERT_DISTANCE = 14.25D; // Max distance expected - crushing head width
  protected static final Double ZERO = 0D;

  public static DistanceMovementType determineMovementType(Queue<Double> distanceHistory) {

    if(distanceHistory != null) {
      Object[] distances = distanceHistory.toArray();
      if(distances.length > 4) {
        Double firstDistance = (Double) distances[0];
        Double averageNewDistances = averageNewest(distanceHistory);

        if(firstDistance != null && averageNewDistances != null) {
          Double distanceGap = Math.abs(firstDistance - averageNewDistances);

          if(distanceGap.compareTo(DISTANCE_SIGNIFICANCE) > 0) {
            if(firstDistance.compareTo(averageNewDistances) < 0) {
              //logger.info("Gap: " + distanceGap + " First: " + firstDistance + "  Average: " + averageNewDistances + " Type: Receding");
              return DistanceMovementType.RECEDING;
            } else {
              //logger.info("Gap: " + distanceGap + " First: " + firstDistance + "  Average: " + averageNewDistances + " Type: Approaching");
              return DistanceMovementType.APPROACHING;
            }
          } else {
            //logger.info("Gap: " + distanceGap + " First: " + firstDistance + "  Average: " + averageNewDistances + " Type: Stopped");
            return DistanceMovementType.STOPPED;
          }
        }
      }
    }

    return DistanceMovementType.UNDETERMINED;
  }

  /**
   *
   * Since we're measuring the crusher moving away from the sensor
   * we want to report the distance the crusher is away from the bottom
   *
   * @param measuredDistance
   * @return
   */
  public static Double invertDistance(Double measuredDistance) {
    if(measuredDistance != null) {
      if(INVERT_DISTANCE.compareTo(measuredDistance) >= 0) {
        return INVERT_DISTANCE - measuredDistance;
      } else {
        return ZERO;
      }
    }

    return null;
  }

  private static Double averageNewest(Queue<Double> distanceHistory) {
    Object[] distances = distanceHistory.toArray();
    int size = distances.length;

    Double distancesAdded = 0D;
    for(int i = 0; i < size; i++) {
      if(i != 0) {
        Double distance = (Double) distances[i];
        distancesAdded = distancesAdded + distance;
      }
    }

    return distancesAdded / (size - 1);
  }
}
