package smushy.sensor.distance.tof;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import smushy.sensor.distance.DistanceSensor;
import smushy.sensor.distance.DistanceSensorBase;

import static smushy.sensor.distance.DistanceUtils.invertDistance;
import static smushy.sensor.distance.tof.TofDistanceSensorUtils.convertMMToInches;

public class TofDistanceSensor extends DistanceSensorBase implements DistanceSensor {

  static final Logger logger = LoggerFactory.getLogger(TofDistanceSensor.class);

  private VL53L0XGpioProvider vl53L0XGpioProvider;

  public TofDistanceSensor(VL53L0XGpioProvider vl53L0XGpioProvider) {
    this.vl53L0XGpioProvider = vl53L0XGpioProvider;
    this.vl53L0XGpioProvider.startMeasuringDistance();
  }

  @Scheduled(fixedDelay = 100)
  protected void setDistance() {
    int sensorDistance = vl53L0XGpioProvider.getDistance();

    if(sensorDistance > 0) {
      distance = convertMMToInches(sensorDistance);
      distance = invertDistance(distance);
      distanceHistory.add(distance);
    } else {
      distance = 0D;
    }
  }
}
