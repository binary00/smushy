package smushy.sensor.distance.tof;


import com.pi4j.io.gpio.GpioProvider;
import com.pi4j.io.gpio.GpioProviderBase;
import com.pi4j.io.i2c.I2CBus;
import com.pi4j.io.i2c.I2CDevice;
import com.pi4j.io.i2c.I2CFactory;

import java.io.IOException;

public class VL53L0XGpioProvider extends GpioProviderBase implements GpioProvider {

  public static final String NAME = "proto.VL53L0XGpioProvider";

  public static final int VL5l0X_ADDRESS_0x29 = 0x29;

  protected static final int VL53L0X_REG_SYSRANGE_START = 0x000;

  protected static final int VL53L0X_REG_RESULT_INTERRUPT_STATUS = 0x0013;
  protected static final int VL53L0X_REG_RESULT_RANGE_STATUS = 0x14;
  protected static final int VL53L0X_REG_SYSTEM_INTERRUPT_CLEAR = 0x000B;

  protected int busNumber;
  protected I2CDevice device;

  @Override
  public String getName() {
    return NAME;
  }

  public VL53L0XGpioProvider(int busNumber) {
    this.busNumber = busNumber;
    initSensor();
  }

  private void initSensor() {

    try {
      I2CBus i2c = I2CFactory.getInstance(busNumber);
      device = i2c.getDevice(VL5l0X_ADDRESS_0x29);

      setVoltageMode();
      setI2CMode();
      clearInterrupt();

    } catch(I2CFactory.UnsupportedBusNumberException e) {
      e.printStackTrace();
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  private void setVoltageMode() throws IOException {
    int response = device.read(0x89);
    response = response | 0x01;

    byte b = (byte) response;
    device.write(0x89, b);
  }

  private void setI2CMode() throws IOException {
    byte b = (byte) 0x00;
    device.write(0x88, b);
  }

  public void startMeasuringDistance() {
    byte b = (byte) 0x02;
    try {
      device.write(VL53L0X_REG_SYSRANGE_START, b);
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public int getDistance() {

    try {
      byte[] distanceBytes = new byte[12];
      device.read(VL53L0X_REG_RESULT_RANGE_STATUS, distanceBytes, 0, 12);
      return ((distanceBytes[10] & 0xFF) << 8) | (distanceBytes[11] & 0xFF);
    } catch(IOException e) {
      e.printStackTrace();
    }

    return -1;
  }

  private void clearInterrupt() throws IOException {
    int loopCount = 0;
    byte interrupt;
    byte zeroed = (byte) 0x07;

    do {
      byte b = (byte) 0x01;
      device.write(VL53L0X_REG_SYSTEM_INTERRUPT_CLEAR, b);

      byte b2 = (byte) 0x00;
      device.write(VL53L0X_REG_SYSTEM_INTERRUPT_CLEAR, b2);

      int interruptData = device.read(VL53L0X_REG_RESULT_INTERRUPT_STATUS);
      interrupt = (byte) interruptData;
      loopCount++;
    } while(((interrupt & zeroed) != 0) && loopCount < 3);
  }
}
