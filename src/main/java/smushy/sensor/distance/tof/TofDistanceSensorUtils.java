package smushy.sensor.distance.tof;


public class TofDistanceSensorUtils {

  protected static final Double CM_TO_INCHES_CONVERSION = 0.0393701D;

  public static Double convertMMToInches(int mmDistance) {

    Double dCMDistance = ((Integer) mmDistance).doubleValue();
    Double inches = CM_TO_INCHES_CONVERSION * dCMDistance;

    return inches;
  }
}
