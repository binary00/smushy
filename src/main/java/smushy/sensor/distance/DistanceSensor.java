package smushy.sensor.distance;


public interface DistanceSensor {

  Double getDistance();
  DistanceMovementType getMovementType();

}
