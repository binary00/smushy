package smushy.sensor.current;


import com.pi4j.gpio.extension.ads.ADS1115GpioProvider;
import com.pi4j.io.gpio.GpioPinAnalogInput;
import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerAnalog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import smushy.operate.actuator.ActuatorOperator;

import static smushy.sensor.current.CurrentSensorType.EXTENDING;
import static smushy.sensor.current.CurrentSensorType.RETRACTING;
import static smushy.sensor.current.CurrentSensorUtils.*;

public class CurrentSensor {

  static final Logger logger = LoggerFactory.getLogger(CurrentSensor.class);

  private Double current;
  private ADS1115GpioProvider ads1115GpioProvider;

  private ActuatorOperator actuatorOperator;
  protected CurrentSensorType lastCurrentSensor;

  public CurrentSensor(GpioPinAnalogInput extendingCurrentSensor, GpioPinAnalogInput retractingCurrentSensor,
                       ADS1115GpioProvider ads1115GpioProvider, ActuatorOperator actuatorOperator) {

    this.ads1115GpioProvider = ads1115GpioProvider;
    this.actuatorOperator = actuatorOperator;

    /*
     *
     * Can't have just one current sensor. It can only monitor on the negative leg of the connection. So when the
     * current alternates to extend/retract the actuator we can have a constant current usage.
     *
     */

    extendingCurrentSensor.addListener((GpioPinListenerAnalog) event -> handleCurrent(event, EXTENDING));
    retractingCurrentSensor.addListener((GpioPinListenerAnalog) event -> handleCurrent(event, RETRACTING));
  }

  protected void handleCurrent(GpioPinAnalogValueChangeEvent event, CurrentSensorType currentSensorType) {
    if(shouldCalculate(actuatorOperator, currentSensorType)) {
      lastCurrentSensor = currentSensorType;

      current = calculateCurrent(event, ads1115GpioProvider);
    }
  }

  public Double getCurrent() {
    return determineCurrent(current, actuatorOperator, lastCurrentSensor);
  }
}
