package smushy.sensor.current;


import com.pi4j.gpio.extension.ads.ADS1115GpioProvider;
import com.pi4j.io.gpio.event.GpioPinAnalogValueChangeEvent;
import smushy.operate.actuator.ActuatorOperator;

class CurrentSensorUtils {

  protected static final Double NO_LOAD_VOLTAGE = 2.5;
  protected static final Double ZERO_OUT_VALUE = 425D;
  protected static final Double MILLI_VOLT_TO_AMP_SCALE = 66D;
  protected static final Double DEFAULT_CURRENT = 0D;

  protected static boolean shouldCalculate(ActuatorOperator actuatorOperator, CurrentSensorType currentSensorType) {
    if(actuatorOperator != null && currentSensorType != null) {
      boolean isExtending = actuatorOperator.isExtending();
      boolean isRetracting = actuatorOperator.isRetracting();

      switch(currentSensorType) {
        case EXTENDING:
          return isExtending;
        case RETRACTING:
          return isRetracting;
      }
    }

    return false;
  }

  protected static Double calculateCurrent(GpioPinAnalogValueChangeEvent event,
                                           ADS1115GpioProvider ads1115GpioProvider) {
    // Reference
    // https://github.com/Pi4J/pi4j/blob/master/pi4j-example/src/main/java/ADS1115GpioExample.java

    Double value = event.getValue();
    value = value - ZERO_OUT_VALUE;

    Double percent = ((value * 100) / ADS1115GpioProvider.ADS1115_RANGE_MAX_VALUE);
    Double amplifiedVoltage = ads1115GpioProvider.getProgrammableGainAmplifier(event.getPin()).getVoltage();
    Double voltage = amplifiedVoltage * (percent / 100);

    Double milliVolts = (voltage - NO_LOAD_VOLTAGE) * 1000;
    Double amps = milliVolts / MILLI_VOLT_TO_AMP_SCALE;

    if(DEFAULT_CURRENT.compareTo(amps) >= 0) {
      return DEFAULT_CURRENT;
    } else {
      return amps;
    }
  }

  protected static Double determineCurrent(Double lastKnownCurrent,
                                           ActuatorOperator actuatorOperator,
                                           CurrentSensorType lastCurrentSensor) {

    // If we're not actuating just return 0, that's what shouldCalculate determines
    if(lastKnownCurrent != null && shouldCalculate(actuatorOperator, lastCurrentSensor)) {
      return lastKnownCurrent;
    } else {
      return DEFAULT_CURRENT;
    }
  }
}
