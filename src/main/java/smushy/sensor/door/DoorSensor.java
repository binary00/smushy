package smushy.sensor.door;


import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import smushy.sensor.BooleanSensor;

import static smushy.utils.TimeUtils.calculateElapsedTimeInSeconds;

public class DoorSensor extends BooleanSensor {

  private long lastOpenedTime = -1;

  public DoorSensor(GpioPinDigitalInput sensorInput) {
    super(sensorInput);
  }

  @Override
  protected void handleCallback(GpioPinDigitalStateChangeEvent event) {
    super.handleCallback(event);
  }

  @Override
  protected void setActivatedData() {
    super.setActivatedData();
    lastOpenedTime = System.currentTimeMillis();
  }

  public int getDoorLastOpenTime() {
    return calculateElapsedTimeInSeconds(lastOpenedTime);
  }
}
